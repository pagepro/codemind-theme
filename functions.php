<?php
/**
 * code-mind functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package code-mind
 */

if ( ! function_exists( 'code_mind_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function code_mind_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on code-mind, use a find and replace
		 * to change 'code-mind' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'code-mind', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'code-mind' ),
			'menu-2' => esc_html__( 'Menu Category', 'code-mind' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'code_mind_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
			'header-text' => array( 'site-title', 'site-description' ),
		) );
	}
endif;
add_action( 'after_setup_theme', 'code_mind_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function code_mind_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'code_mind_content_width', 640 );
}
add_action( 'after_setup_theme', 'code_mind_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function code_mind_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Social Media', 'code-mind' ),
		'id'            => 'social-media',
		'description'   => esc_html__( 'Add widgets here.', 'code-mind' ),
		'before_widget' => '<ul class="c-socials-list">',
		'after_widget'  => '</ul>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Widgets', 'code-mind' ),
		'id'            => 'widgets',
		'description'   => esc_html__( 'Add widgets here.', 'code-mind' ),
		'before_widget' => '<div class="c-sidebar-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="c-categories-nav__title theme-color-2 t-upper theme-font-1 theme-size-5 theme-weight-1 theme-l-height-4">',
		'after_title'   => '<p>',
	) );
}
add_action( 'widgets_init', 'code_mind_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function code_mind_scripts() {
	// wp_enqueue_style( 'code-mind-style', get_stylesheet_uri() );
	wp_enqueue_style( 'code-mind-grid-style', get_template_directory_uri() . '/static/css/grid.css' );
	$template_name = '';
	
	if ( is_page_template( 'template-reactpost.php' ) ) {
		wp_enqueue_style( 'code-mind-custom-style-reactpost', get_template_directory_uri() . '/static/css/main-reactpost.css' );
		wp_enqueue_script( 'code-mind-custom-script-reactpost', get_template_directory_uri() . '/static/js/app-reactpost.js', array(), filemtime( get_template_directory() . '/static/js/app-reactpost.js' ), true );
		$template_name = 'code-mind-custom-script-reactpost';
	} else {
		wp_enqueue_style( 'code-mind-custom-style', get_template_directory_uri() . '/static/css/main.css' );
		wp_enqueue_script( 'code-mind-custom-script', get_template_directory_uri() . '/static/js/app.js', array(), filemtime( get_template_directory() . '/static/js/app.js' ), true );
		$template_name = 'code-mind-custom-script';
	}
	wp_localize_script($template_name, 'valid', array(
		'warningMessage' => __('Insert value in field-name', 'code-mind'),
		'incorrectEmail' => __('Insert correct email', 'code-mind'),
		'emptyEmail' => __('Insert email', 'code-mind'),
		'correctEmail' => __('Thank you', 'code-mind'),
		'error' => __('Error', 'code-mind'),
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
	));
	wp_enqueue_script( 'code-mind-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), filemtime( get_template_directory() . '/js/skip-link-focus-fix.js' ), true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'code_mind_scripts' );


function load_custom_wp_admin_script() {
	wp_enqueue_script( 'custom_wp_admin_js', get_template_directory_uri() . '/js/admin-script.js', false, '1.0.0' );
	wp_enqueue_script( 'custom_wp_admin_js' );
	wp_enqueue_style( 'custom_social-widget-css', get_template_directory_uri() . '/layouts/social-media-widget.css' );
}

add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_script' );

/**
* Add the 'blog-link' class to the output of the_author_posts_link()
*/
add_filter( 'the_author_posts_link', function( $link ) {
	return str_replace( 'rel="author"', 'rel="author" class="c-post-info__author__name theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 t-upper t-space-wide"', $link );
});

/**
 * Styles for default gutenberg blocks
 */
require get_template_directory() . '/inc/custom-styles-gutenberg.php';


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Menu
 */
require get_template_directory() . '/inc/menu.php';

/**
 * Widgets
 */
require get_template_directory() . '/inc/custom-widgets.php';

/**
 * Comment form validation
 */
require get_template_directory() . '/inc/comment-validation.php';

/**
 * Newsletter custom post type
 */
require get_template_directory() . '/inc/newsletter.php';

/**
 * Custom fields in user profile
 */
require get_template_directory() . '/inc/custom-fields-user.php';

/**
 * Custom admin menu
 */
require get_template_directory() . '/inc/admin-menu-custom.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
