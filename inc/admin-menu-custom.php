<?php
/**
 * Register a custom menu page.
 */
function register_theme_options_page() {
    add_menu_page(
        __( 'Theme settings', 'codemind' ),
        'Theme settings',
        'manage_options',
        'themesettings',
        'theme_settings_page',
        'dashicons-welcome-widgets-menus',
        90
    );
}
add_action( 'admin_menu', 'register_theme_options_page' );

/**
 * Display a custom menu page
 */
function theme_settings_page() {
    $checked = '';
    if ( isset( $_POST['submit'] ) ) {
        $value = $_POST['metatagstheme'];
        update_option( 'metatagstheme', $value );
    }
    $value = get_option( 'metatagstheme', '1' );
    $checked = ( $value == '1' ) ? 'checked' : '';
    echo '<div class="wrap"><h1>Theme settings</h1><form method="post"> ';
    echo '<label for="metatagstheme">Show meta tags from theme?</label>';
    echo '<div><input type="checkbox" id="metatagstheme" name="metatagstheme" value="1" ' . $checked . '></div>';
    echo '<input type="submit" value="Save" class="button button-primary" name="submit">';
    echo '</form></div>';
}

