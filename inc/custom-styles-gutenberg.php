<?php

add_action('enqueue_block_editor_assets', 'codemind_gutenberg_editor_assets' );

function codemind_gutenberg_editor_assets() {
  wp_enqueue_style('codemind-styles-gutenberg', get_template_directory_uri() . '/static/css/main.css', FALSE);
}