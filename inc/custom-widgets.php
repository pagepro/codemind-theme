<?php
class SocialMedia extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(
            'social_media_widget',
            __('Social Media Widget', 'code-mind'),
            [
                'classname'   => 'social_media_widget',
                'description' => __('Select social media', 'code-mind')
            ]
        );
        load_plugin_textdomain('thisisanewblock', false, basename(dirname(__FILE__)) . '/languages');
    }

    public function form($instance)
    {

        $url = array(
            array(
                'name' => 'twitter',
                'url' => $instance['url_twitter'],
                'checkbox' => $instance['url_twitter_check']
            ),

            array(
                'name' => 'facebook',
                'url' => $instance['url_facebook'],
                'checkbox' => $instance['url_facebook_check']
            ),

            array(
                'name' => 'pinterest',
                'url' => $instance['url_pinterest'],
                'checkbox' => $instance['url_pinterest_check']
            ),

            array(
                'name' => 'instagram',
                'url' => $instance['url_instagram'],
                'checkbox' => $instance['url_instagram_check']
            ),

            array(
                'name' => 'snapchat',
                'url' => $instance['url_snapchat'],
                'checkbox' => $instance['url_snapchat_check']
            ),

        );

        $paragraph = esc_attr($instance['paragraph']);

        ?>

        <?php foreach ($url as $social) : ?>
            <p>
                <input type="checkbox" onclick="social_check(this, '<?php echo $social['name']; ?>');" <?php checked($social['checkbox'], 'on'); ?> name="<?php echo $this->get_field_name('url_' . $social['name'] . '_check'); ?>" id="<?php echo $this->get_field_id('url_' . $social['name'] . '_check'); ?>" />
                <svg class="o-icon o-icon--<?php echo $social['name']; ?>">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#<?php echo $social['name']; ?>"></use>
                </svg>
                <label for=”<?php echo $this->get_field_id('url_' . $social['name'] . '_check'); ?>”><?php _e(ucfirst($social['name'])); ?></label>
            </p>
            <p id="widget-social-url--<?php echo $social['name']; ?>" style="<?php echo ($social['checkbox'] == 'on') ? 'display: block;' : 'display: none;'; ?>">
                <label for=”<?php echo $this->get_field_id('url_' . $social['name']); ?>”><?php _e(ucfirst($social['name']) . ' profile URL:'); ?></label>
                <br>
                <input type="text" value="<?php echo $social['url']; ?>" name="<?php echo $this->get_field_name('url_' . $social['name']); ?>" id="<?php echo $this->get_field_id('url_' . $social['name']); ?>" />
                <br />
            </p>
        <?php endforeach; ?>

<?php
    }

    public function widget($args, $instance)
    {
        extract($args);

        $url = array(
            array(
                'name' => 'twitter',
                'url' => $instance['url_twitter'],
                'checkbox' => $instance['url_twitter_check']
            ),

            array(
                'name' => 'facebook',
                'url' => $instance['url_facebook'],
                'checkbox' => $instance['url_facebook_check']
            ),

            array(
                'name' => 'pinterest',
                'url' => $instance['url_pinterest'],
                'checkbox' => $instance['url_pinterest_check']
            ),

            array(
                'name' => 'instagram',
                'url' => $instance['url_instagram'],
                'checkbox' => $instance['url_twitter_check']
            ),

            array(
                'name' => 'snapchat',
                'url' => $instance['url_snapchat'],
                'checkbox' => $instance['url_snapchat_check']
            ),

        );

        echo $before_widget;

        foreach ($url as $social) {

            if ($social['checkbox'] == 'on' && $social['url']) {
                echo '
        <li class="c-socials-list__item">
          <a href="' . $social['url'] . '" class="c-socials-list__item__link" target="_blank">
            <svg class="o-icon o-icon--twitter">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' . get_template_directory_uri() . '/static/symbol/svg/sprite.symbol.svg#' . $social['name'] . '"></use>
            </svg>
          </a>
        </li>
        ';
            }
        }

        echo $after_widget;
    }

    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;

        $instance['url_twitter'] = strip_tags($new_instance['url_twitter']);
        $instance['url_twitter_check'] = strip_tags($new_instance['url_twitter_check']);

        $instance['url_facebook'] = strip_tags($new_instance['url_facebook']);
        $instance['url_facebook_check'] = strip_tags($new_instance['url_facebook_check']);

        $instance['url_pinterest'] = strip_tags($new_instance['url_pinterest']);
        $instance['url_pinterest_check'] = strip_tags($new_instance['url_pinterest_check']);

        $instance['url_instagram'] = strip_tags($new_instance['url_instagram']);
        $instance['url_instagram_check'] = strip_tags($new_instance['url_instagram_check']);

        $instance['url_snapchat'] = strip_tags($new_instance['url_snapchat']);
        $instance['url_snapchat_check'] = strip_tags($new_instance['url_snapchat_check']);

        return $instance;
    }
}

add_action('widgets_init', function () {
    register_widget('SocialMedia');
});
