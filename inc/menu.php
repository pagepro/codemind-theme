<?php
class Code_Mind_Walker_Nav_Menu extends Walker_Nav_Menu {
  function start_el( &$output, $item, $depth=0, $args=array(), $id = 0 ) {
    $object = $item->object;
    $type = $item->type;
    $title = $item->title;
    $description = $item->description;
    $permalink = $item->url;
  
    $active = ( in_array( 'current-menu-item', $item->classes ) || in_array( 'current_page_item', $item->classes ) || in_array( 'current_page_parrent', $item->classes ) ) ? 'is-active' : '';

    if(get_post_type() == 'post'){
      $active = 'is-active';
    }
    if($type == 'taxonomy'){
      $output .= "<li class='c-categories-nav-list__item $active'>";
    }else{
      $output .= "<li class='c-main-nav-list__item $active'>";
    }

    if( $permalink && $permalink != '#' ) {
      if($type == 'taxonomy'){
        $output .= '<a href="' . $permalink . '" class="c-main-nav-list__item__link theme-color-2 theme-l-height-1"><span class="theme-font-1 theme-weight-1">';
      }else {
        $output .= '<a href="' . $permalink . '" class="c-main-nav-list__item__link theme-color-2 theme-l-height-1"><span class="theme-font-1 theme-size-8 theme-weight-2">';
      }
    } else {
      $output .= '<span>';
    }

    $output .= $title;

    if( $description != '' && $depth == 0 && !in_array('menu-item-type-post_type_archive', $item->classes) ) {
      	$output .= '<small class="description">' . $description . '</small>';
    }

    if( $permalink && $permalink != '#' ){
        $output .= '</span></a>';
    }else{
        $output .= '</span>';
    }

    $output .= '</li>';

  }
}
