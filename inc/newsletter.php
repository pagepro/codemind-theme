<?php

// Register Custom Post Type Newsletter
function newsletter_codemind_post_type() {

	$labels = array(
		'name'                  => _x( 'Newsletter', 'Post Type General Name', 'code-mind' ),
		'singular_name'         => _x( 'Newsletter', 'Post Type Singular Name', 'code-mind' ),
		'menu_name'             => __( 'Newsletter', 'code-mind' ),
		'name_admin_bar'        => __( 'Newsletter', 'code-mind' ),
		'archives'              => __( 'Item Archives', 'code-mind' ),
		'attributes'            => __( 'Item Attributes', 'code-mind' ),
		'parent_item_colon'     => __( 'Parent Item:', 'code-mind' ),
		'all_items'             => __( 'All Items', 'code-mind' ),
		'add_new_item'          => __( 'Add New Item', 'code-mind' ),
		'add_new'               => __( 'Add New', 'code-mind' ),
		'new_item'              => __( 'New Item', 'code-mind' ),
		'edit_item'             => __( 'Edit Item', 'code-mind' ),
		'update_item'           => __( 'Update Item', 'code-mind' ),
		'view_item'             => __( 'View Item', 'code-mind' ),
		'view_items'            => __( 'View Items', 'code-mind' ),
		'search_items'          => __( 'Search Item', 'code-mind' ),
		'not_found'             => __( 'Not found', 'code-mind' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'code-mind' ),
		'featured_image'        => __( 'Featured Image', 'code-mind' ),
		'set_featured_image'    => __( 'Set featured image', 'code-mind' ),
		'remove_featured_image' => __( 'Remove featured image', 'code-mind' ),
		'use_featured_image'    => __( 'Use as featured image', 'code-mind' ),
		'insert_into_item'      => __( 'Insert into item', 'code-mind' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'code-mind' ),
		'items_list'            => __( 'Items list', 'code-mind' ),
		'items_list_navigation' => __( 'Items list navigation', 'code-mind' ),
		'filter_items_list'     => __( 'Filter items list', 'code-mind' ),
	);

	$args = array(
		'label'                 => __( 'Newsletter', 'code-mind' ),
		'description'           => __( 'Post type newsletter', 'code-mind' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'hierarchical'          => false,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-email-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => false,
		'capability_type'       => 'page',
	);
	register_post_type( 'newsletter', $args );

}
add_action( 'init', 'newsletter_codemind_post_type', 0 );

function newsletter_codemind_action() {

	if ( isset ( $_POST['newsletter-email'] ) ) {
		$email = sanitize_email( $_POST['newsletter-email'] );
		$campaign = sanitize_text_field( $_POST['newsletter-campaign'] );

		$post_data = array (
			'post_title' => $email,
			'post_type' => 'newsletter',
			'post_status' => 'publish'
		);

		$post_id = wp_insert_post( $post_data );

		if ( $post_id ) {
			update_post_meta( $post_id, 'newsletter-email', $_POST['newsletter-email'], $email );
			update_post_meta( $post_id, 'newsletter-campaign', $_POST['newsletter-campaign'], $campaign );
			echo json_encode( $post_id );
		}
	}

    exit;
}
add_action( 'wp_ajax_newsletter', 'newsletter_codemind_action' );
add_action( 'wp_ajax_nopriv_newsletter', 'newsletter_codemind_action' );

function codemind_custom_newsletter_columns( $columns ) {
	$new_column = array( 'Campaign' => __( 'Campaign', 'code-mind') );
	array_splice( $columns, 2, 0, $new_column );
	return $columns;
}
add_filter( 'manage_newsletter_posts_columns' , 'codemind_custom_newsletter_columns' );

function codemind_fill_custom_post_type_columns( $column, $post_id ) {
	switch ( $column ) {
		case 'Campaign' :
			$campaign =  get_post_meta( $post_id , 'newsletter-campaign' , true );
			if ( !$campaign ) {
				update_post_meta( $post_id, 'newsletter-campaign', 'general', $campaign );
			}
			echo '<a href="' . admin_url() . 'edit.php?post_type=newsletter&newsletter-campaign=' . $campaign . '">' . $campaign . '</a>';
			break;
	}
}
add_action( 'manage_newsletter_posts_custom_column' , 'codemind_fill_custom_post_type_columns', 10, 2 );

function  codemind_prefix_parse_filter( $query ) {
	global $pagenow;
	$current_page = isset( $_GET['post_type'] ) ? $_GET['post_type'] : '';

	if ( is_admin() && 'newsletter' == $current_page && 'edit.php' == $pagenow && isset( $_GET['newsletter-campaign'] ) ) {
		$competition_name = $_GET['newsletter-campaign'];
		$query->query_vars['meta_key'] = 'newsletter-campaign';
		$query->query_vars['meta_value'] = $competition_name;
		$query->query_vars['meta_compare'] = '=';
	}
}
add_filter( 'parse_query', 'codemind_prefix_parse_filter' );
