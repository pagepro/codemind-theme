<?php

add_action( 'comment_form', 'codemind_comment_nonce_to_form' );

function codemind_comment_nonce_to_form() {
    wp_nonce_field( 'comment_nonce', 'add_comment' );
}

add_action( 'wp_ajax_ajaxcomments', 'codemind_submit_ajax_comment' );
add_action( 'wp_ajax_nopriv_ajaxcomments', 'codemind_submit_ajax_comment' );
 
function codemind_submit_ajax_comment() {
	if(isset($_POST['comment'])) {
		$post_comment = sanitize_text_field( $_POST['comment'] );
		update_post_meta( $_POST['comment_post_ID'], 'comment', $post_comment );
	}
	
	if(isset($_POST['author'])) {
		$post_author = sanitize_text_field( $_POST['author'] );
		update_post_meta( $_POST['comment_post_ID'], 'author', $post_author );
	}
	
	if(isset($_POST['email'])) {
		$post_email = sanitize_text_field( $_POST['email'] );
		update_post_meta( $_POST['comment_post_ID'], 'email', $post_email );
	}
	
	if(isset($_POST['url'])) {
		$post_url = sanitize_text_field( $_POST['url'] );
		update_post_meta( $_POST['comment_post_ID'], 'url', $post_url );
	}
	
	$comment = wp_handle_comment_submission( wp_unslash( $_POST ) );
	if ( is_wp_error( $comment ) || !wp_verify_nonce( $_POST['add_comment'], 'comment_nonce' )  ) {
		$error_data = intval( $comment->get_error_data() );
		if ( ! empty( $error_data ) ) {
			wp_die( '<p>' . $comment->get_error_message() . '</p>', __( 'Comment Submission Failure', 'codemind' ), array( 'response' => $error_data, 'back_link' => true ) );
		} else {
			wp_die( __( 'Unknown error', 'codemind' ) );
		}
	}
	$user = wp_get_current_user();
	do_action('set_comment_cookies', $comment, $user);
 
	$comment_depth = 1;
	$comment_parent = $comment->comment_parent;
	while( $comment_parent ){
		$comment_depth++;
		$parent_comment = get_comment( $comment_parent );
		$comment_parent = $parent_comment->comment_parent;
	}
 
	?>
	<?php if ($comment_depth == 1) : ?>
		<li class="c-comments-list__item js-comments-item">
			<div class="c-comment">
				<div class="c-comment__avatar">
					<figure class="o-img-wrapper">
						<img src="<?php echo get_avatar_url($comment->comment_author_email) ?>" alt="<?php echo $comment->comment_author; ?>" />
					</figure>
				</div>
				<div class="c-comment__content">
					<div class="c-comment__content__top">
						<div class="c-comment__content__top__author">
							<p class="theme-weight-2 theme-size-5"><?php echo $comment->comment_author; ?></p>
							<span class="theme-color-5 theme-weight-2 theme-size-4">
								<?php _e('Posted on', 'codemind'); ?>
								<time class="theme-color-4" datetime="<?php echo get_the_date('Y-m-d H:i'); ?>"><?php echo $comment->comment_date; ?></time>
							</span>
						</div>
						<div class="c-comment__content__top__action">
							<?php
							$max_depth = get_option('thread_comments_depth');
							$default = array(
								'add_below'  => 'comment',
								'respond_id' => 'respond',
								'reply_text' => '<span class="s-cta-2 theme-bg-5 theme-color-4 theme-size-2 theme-weight-2 js-comment-reply" data-comment-reply-text="'.__('Leave a Reply to comment', 'codemind').'" data-comment-reply-cancel="'.__('Cancel reply', 'codemind').'">' . __('Reply') . '</span>',
								'login_text' => __('Log in to Reply'),
								'depth'      => 1,
								'before'     => '',
								'after'      => '',
								'max_depth'  => $max_depth
							);
							comment_reply_link($default, $comment->comment_ID, $comment->comment_post_ID);
							?>
						</div>
					</div>
					<div class="c-comment__content__desc theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4">
						<p><?php echo $comment->comment_content; ?></p>
					</div>
				</div>
			</div>
		</li>
	<?php endif; ?>

	<?php if ($comment_depth > 1) : ?>
			<li class="c-reply-list__item">
				<div class="c-comment">
					<div class="c-comment__avatar">
						<figure class="o-img-wrapper">
							<img src="<?php echo get_avatar_url($comment->comment_author_email) ?>" alt="<?php echo $comment->comment_author; ?>" />
						</figure>
					</div>
					<div class="c-comment__content">
						<div class="c-comment__content__top">
							<div class="c-comment__content__top__author">
								<p class="theme-weight-2 theme-size-5"><?php echo $comment->comment_author; ?></p>
								<span class="theme-color-5 theme-weight-2 theme-size-4">
									<?php _e('Posted on', 'codemind'); ?>
									<time class="theme-color-4" datetime="<?php echo get_the_date('Y-m-d H:i'); ?>"><?php echo $comment->comment_date; ?></time>
								</span>
							</div>
						</div>
						<div class="c-comment__content__desc theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4">
							<p><?php echo $comment->comment_content; ?></p>
						</div>
					</div>
				</div>
			</li>
	<?php endif; ?>


	<?php die(); ?>
<?php } ?>
