<?php

add_action( 'show_user_profile', 'user_social_fields_codemind' );
add_action( 'edit_user_profile', 'user_social_fields_codemind' );

function user_social_fields_codemind( $user ) { ?>
    <h3><?php _e( 'Social media', 'code-mind' ); ?></h3>
    <table class="form-table">
        <tr>
            <th>
                <label for="twitter">
                    <?php _e( 'Twitter URL', 'code-mind' ); ?>
                </label>
            </th>
            <td>
                <input type="text" name="twitter" id="twitter" value="<?php echo esc_attr(get_the_author_meta('twitter', $user->ID)); ?>" class="regular-text" /><br />
            </td>
        </tr>
        <tr>
            <th>
                <label for="facebook">
                    <?php _e( 'Facebook URL', 'code-mind' ); ?>
                </label>
            </th>
            <td>
                <input type="text" name="facebook" id="facebook" value="<?php echo esc_attr(get_the_author_meta('facebook', $user->ID)); ?>" class="regular-text" /><br />
            </td>
        </tr>
        <tr>
            <th>
                <label for="instagram">
                    <?php _e( 'Instagram URL', 'code-mind' ); ?>
                </label>
            </th>
            <td>
                <input type="text" name="instagram" id="instagram" value="<?php echo esc_attr(get_the_author_meta('instagram', $user->ID)); ?>" class="regular-text" /><br />
            </td>
        </tr>
        <tr>
            <th>
                <label for="pinterest">
                    <?php _e( 'Pinterest URL', 'code-mind' ); ?>
                </label>
            </th>
            <td>
                <input type="text" name="pinterest" id="pinterest" value="<?php echo esc_attr(get_the_author_meta('pinterest', $user->ID)); ?>" class="regular-text" /><br />
            </td>
        </tr>
        <tr>
            <th>
                <label for="snapchat">
                    <?php _e( 'Snpachat URL', 'code-mind' ); ?>
                </label>
            </th>
            <td>
                <input type="text" name="snapchat" id="snapchat" value="<?php echo esc_attr(get_the_author_meta('snapchat', $user->ID)); ?>" class="regular-text" /><br />
            </td>
        </tr>
    </table>
<?php }

add_action( 'personal_options_update', 'save_user_social_fields_codemind' );
add_action( 'edit_user_profile_update', 'save_user_social_fields_codemind' );

function save_user_social_fields_codemind( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) {
        return false;
    }
    update_user_meta( $user_id, 'facebook', $_POST['facebook'] );
    update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
    update_user_meta( $user_id, 'instagram', $_POST['instagram'] );
    update_user_meta( $user_id, 'pinterest', $_POST['pinterest'] );
    update_user_meta( $user_id, 'snapchat', $_POST['snapchat'] );
}

?>
