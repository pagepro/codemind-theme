<?php

/**
 * code-mind Theme Customizer
 *
 * @package code-mind
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function code_mind_customize_register($wp_customize)
{
	$wp_customize->get_setting('blogname')->transport         = 'postMessage';
	$wp_customize->get_setting('blogdescription')->transport  = 'postMessage';
	$wp_customize->get_setting('header_textcolor')->transport = 'postMessage';

	if (isset($wp_customize->selective_refresh)) {
		$wp_customize->selective_refresh->add_partial('blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'code_mind_customize_partial_blogname',
		));
		$wp_customize->selective_refresh->add_partial('blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'code_mind_customize_partial_blogdescription',
		));
	}
}
add_action('customize_register', 'code_mind_customize_register');

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function code_mind_customize_partial_blogname()
{
	bloginfo('name');
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function code_mind_customize_partial_blogdescription()
{
	bloginfo('description');
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function code_mind_customize_preview_js()
{
	wp_enqueue_script('code-mind-customizer', get_template_directory_uri() . '/js/customizer.js', array('customize-preview'), '20151215', true);
}
add_action('customize_preview_init', 'code_mind_customize_preview_js');

add_action('customize_register', 'codemind_register_theme_customizer');

function codemind_register_theme_customizer($wp_customize)
{

	$wp_customize->add_panel('text_blocks', array(
		'priority'       => 500,
		'theme_supports' => '',
		'title'          => __('Footer', 'codemind'),
	));

	$wp_customize->add_section('custom_footer_text', array(
		'title'    => __('Change Footer Text', 'codemind'),
		'panel'    => 'text_blocks',
		'priority' => 10
	));

	$wp_customize->add_setting('footer_text_block', array(
		'default'           => __('default footer', 'codemind'),
	));

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'custom_footer_text_left',
			array(
				'label'    => __('Footer Text left', 'codemind'),
				'section'  => 'custom_footer_text',
				'settings' => 'footer_text_block',
				'type'     => 'textarea'
			)
		)
	);

	$wp_customize->add_section('custom_newsletter_text', array(
		'title'    => __('Change Newsletter Text', 'codemind'),
		'panel'    => 'text_blocks',
		'priority' => 20
	));

	$wp_customize->add_setting('newsletter_text_block', array(
		'default'           => __('default newsletter', 'codemind'),
	));

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'custom_newsletter_text_left',
			array(
				'label'    => __('Newslterr Text right', 'codemind'),
				'section'  => 'custom_newsletter_text',
				'settings' => 'newsletter_text_block',
				'type'     => 'textarea'
			)
		)
	);

	$wp_customize->add_setting('alt_logo', array(
		'height'      => 100,
		'width'       => 200,
		'flex-height' => true,
		'flex-width'  => true,
	));

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'alt_logo',
			array(
				'label'    => __('Alternative logo', 'codemind'),
				'section'  => 'title_tagline',
				'settings' => 'alt_logo',
			)
		)
	);
}
