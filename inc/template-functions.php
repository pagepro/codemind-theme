<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package code-mind
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function code_mind_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'code_mind_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function code_mind_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'code_mind_pingback_header' );

add_action( 'admin_menu', 'my_create_post_meta_box' );
add_action( 'save_post', 'my_save_post_meta_box', 10, 2 );

function my_create_post_meta_box() {
    add_meta_box( 'my-meta-box', 'Heading Excerpt', 'my_post_meta_box', 'post', 'normal', 'high' );
}

function my_post_meta_box( $object, $box ) { ?>
    <p>
        <label for="second-excerpt"><?php _e('Heading Excerpt','code-mind') ?></label>
        <textarea name="second-excerpt" id="second-excerpt" cols="60" rows="4" tabindex="30" style="width: 97%;"><?php echo wp_specialchars( get_post_meta( $object->ID, 'Second Excerpt', true ), 1 ); ?></textarea>
        <input type="hidden" name="my_meta_box_nonce" value="<?php echo wp_create_nonce( plugin_basename( __FILE__ ) ); ?>" />
    </p>
<?php }

function my_save_post_meta_box( $post_id, $post ) {

    if ( !wp_verify_nonce( $_POST['my_meta_box_nonce'], plugin_basename( __FILE__ ) ) )
        return $post_id;

    if ( !current_user_can( 'edit_post', $post_id ) )
        return $post_id;

    $meta_value = get_post_meta( $post_id, 'Second Excerpt', true );
    $new_meta_value = stripslashes( $_POST['second-excerpt'] );

    if ( $new_meta_value && '' == $meta_value )
        add_post_meta( $post_id, 'Second Excerpt', $new_meta_value, true );

    elseif ( $new_meta_value != $meta_value )
        update_post_meta( $post_id, 'Second Excerpt', $new_meta_value );

    elseif ( '' == $new_meta_value && $meta_value )
        delete_post_meta( $post_id, 'Second Excerpt', $meta_value );
}

add_action('wp_head', 'codemind_opengraph');
function codemind_opengraph() {
    $check_open_graph = get_option( 'metatagstheme' );
    global $post;
    if ( is_single() && $check_open_graph == '1' ) {
        if ( has_post_thumbnail( $post->ID ) ) {
            $img_src = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'full')[0];
        } else {
            $img_src = get_template_directory_uri() . '/static/img/logo.png';
        }

        $excerpt = get_the_excerpt();

        if ( $excerpt ) {
            $excerpt = esc_attr( strip_tags( $excerpt ) );
            $excerpt = str_replace( "", "'", $excerpt );
        } else {
            $excerpt = get_bloginfo( 'description' );
        }

        echo '<meta property="og:title" content="' . get_the_title() . '"/>';
        echo '<meta property="og:description" content="' . $excerpt . '"/>';
        echo '<meta property="og:type" content="article"/>';
        echo '<meta property="og:url" content="' . get_the_permalink() . '"/>';
        echo '<meta property="og:site_name" content="' . get_bloginfo() . '"/>';
        echo '<meta property="og:image" content="' . $img_src . '"/>';
        echo '<meta property="og:image:width" content="600"/>';
        echo '<meta property="og:image:height" content="315"/>';
        echo '<meta name="description" content="Blog about React, React Native, Gatsby, and front-end development">';
    } else {
        return;
    }
}

function codemind_exclude_from_search( $query ) {
    if ( $query->is_search ) {
        $query->set( 'post_type', 'post' );
    }
    return $query;
}
add_filter( 'pre_get_posts','codemind_exclude_from_search' );

/**
 * AMP
 */
function codemind_amp_custom_template( $template, $template_type, $post ) {
    if ( 'single' === $template_type ) {
      $template = get_template_directory() . '/templates/amp/single.php';
    }
    if ( 'header-bar' === $template_type ) {
      $template = get_template_directory() . '/templates/amp/header-bar.php';
    }
    return $template;
}
add_filter( 'amp_post_template_file', 'codemind_amp_custom_template', 10, 3 );
