var config = require('../config')
var gulp = require('gulp')
var gulpSequence = require('gulp-sequence')
var getEnabledTasks = require('../lib/getEnabledTasks')

const productionTask = callback => {
  global.production = true
  const tasks = getEnabledTasks('production')
  gulpSequence('clean',
    tasks.assetTasks,
    tasks.codeTasks,
    'critical',
    config.tasks.production.rev ? 'rev' : false,
    'size-report',
    callback)
}

gulp.task('production', productionTask)
module.exports = productionTask
