const SCSS_VARIABLES = {
  inner: 1240,
  gutter: 20
}

const BREAKPOINTS = {
  desktopSmall: SCSS_VARIABLES.inner + SCSS_VARIABLES.gutter * 4,
  tablet: 1024,
  phone: 767,
  phoneSmall: 320
}

const isDesktop = () => window.matchMedia(`(min-width: ${BREAKPOINTS.tablet}px)`).matches

const convertToKebabCase = string => string.replace(/([a-z])([A-Z])/g, '$1-$2').replace(/\s+/g, '-').toLowerCase()

module.exports = {
  BREAKPOINTS,
  SCSS_VARIABLES,
  convertToKebabCase,
  isDesktop
}
