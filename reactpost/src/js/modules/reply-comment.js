import $ from 'jquery'

const $el = {
  parentContainer: $('#comment_parent')
}

const classes = {
  messageContainer: $('.js-reply-message')
}

const replyComment = e => {
  e.preventDefault()
  const $target = $(e.currentTarget)
  const $child = $target.find('.js-comment-reply')
  const commentId = $target.attr('data-commentid')
  const replyText = $child.attr('data-comment-reply-text')
  const replyCancel = $child.attr('data-comment-reply-cancel')

  $el.parentContainer.val(commentId)
  classes.messageContainer.text(replyText).append('<small><a rel="nofollow" id="cancel-comment-reply-link" href="."> ' + replyCancel + '</a></small>')
  $('html, body').animate({
    scrollTop: $('#respond').offset().top
  })
}

const defaultComment = e => {
  e.preventDefault()
  $el.parentContainer.val(0)
  classes.messageContainer.text(classes.messageContainer.attr('data-comment-reply'))
}

$(document).on('click', '.comment-reply-link', e => {
  replyComment(e)
})

$(document).on('click', '#cancel-comment-reply-link', e => {
  defaultComment(e)
})
