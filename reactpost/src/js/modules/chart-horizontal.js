import $ from 'jquery'
import ScrollMagic from 'scrollmagic/scrollmagic/minified/ScrollMagic.min.js'
import Chart from 'chart.js'
// eslint-disable-next-line
import ChartDataLabels from 'chartjs-plugin-datalabels';

// Debugging
// import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators' // Debugging

const controller = new ScrollMagic.Controller()
const selectors = {
  chartContainer: '.js-chart-graph'
}

const chartHorizontal = {
  init: function () {
    const charts = $(selectors.chartContainer)
    if (!charts.length) return

    charts.each(function (index, item) {
      const ctx = $(this).children('canvas')

      const statisticsRaw = $(this).data('statistics')
      const statistics = JSON.parse(statisticsRaw)

      const currentDatasetValues = chartHorizontal.getDatasetValues(statistics)
      const currentDatasetLabels = chartHorizontal.getDatasetLabels(statistics)
      const currentLabelsDecors = chartHorizontal.getLabelsDecor(statistics)
      const currentMaxValue = Math.max(...currentDatasetValues)

      // eslint-disable-next-line
      const myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
          labels: currentDatasetLabels,
          datasets: [{
            barPercentage: 0.8,
            categoryPercentage: 0.8,
            data: currentDatasetValues,
            backgroundColor: currentLabelsDecors,
            borderWidth: 0
          }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          legend: {
            display: false
          },
          tooltips: {
            enabled: false
          },
          scales: {
            xAxes: [{
              gridLines: {
                display: false
              },
              ticks: {
                display: false,
                beginAtZero: true,
                max: currentMaxValue * 1.6
              }
            }],
            yAxes: [{
              gridLines: {
                display: false
              },
              ticks: {
                fontSize: '14',
                fontColor: '#585858',
                callback: function (value, index, values) {
                  return statistics[index][0]
                }
              }
            }]
          },
          plugins: {
            datalabels: {
              align: 'end',
              anchor: 'end',
              borderRadius: 4,
              color: 'black',
              fontStyle: 'bold',
              formatter: function (value, test) {
                return currentDatasetLabels[test.dataIndex]
              }
            }
          },
          animation: {
            easing: 'easeInOutExpo',
            duration: 2000
          }
        }
      })
      myChart.reset()

      new ScrollMagic.Scene({
        triggerHook: item.dataset.triggerHook || 0.7,
        triggerElement: item
      })
        .on('enter', () => myChart.update())
        .addTo(controller)
        // .addIndicators() // Debugging
    })
  },
  getDatasetValues: function (data) {
    return data.reduce(function (prev, item) {
      const newTable = prev
      let newItem = item[0].replace('%', '').replace(',', '')
      newTable.push(parseInt(newItem, 10))
      return newTable
    }, [])
  },
  getDatasetLabels: function (data) {
    return data.reduce(function (prev, item) {
      const newTable = prev
      newTable.push(item[1])
      return newTable
    }, [])
  },
  getLabelsDecor: function (data) {
    return data.reduce(function (prev, item) {
      const newTable = prev
      const currentColor = item[2] === 'active' ? 'rgb(10, 43, 61)' : 'rgb(150, 173, 186)'
      newTable.push(currentColor)
      return newTable
    }, [])
  }
}

$(document).on('ready', function () {
  chartHorizontal.init()
})
