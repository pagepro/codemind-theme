import $ from 'jquery'
import ScrollMagic from 'scrollmagic/scrollmagic/minified/ScrollMagic.min.js'

// Debugging
// import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators' // Debugging

const controller = new ScrollMagic.Controller()
const elSelectors = {
  trigger: '.ui-animation-hook'
}

const animationHooks = {
  init: function () {
    $(elSelectors.trigger).each(function (index, item) {
      new ScrollMagic.Scene({
        triggerHook: item.dataset.triggerHook || 0.7,
        triggerElement: item
        // duration: $(this).outerHeight(true)
      })
        .on('enter', () => $(item).addClass('is-active'))
        .addTo(controller)
        // .addIndicators() // Debugging
    })
  }
}

animationHooks.init()
