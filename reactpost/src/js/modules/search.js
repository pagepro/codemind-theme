import $ from 'jquery'

const css = {
  activeClass: 'is-active',
  blockedClass: 'is-blocked'
}
const $el = {
  headerSearchToggler: $('.js-search-toggler'),
  search: $('.js-search'),
  searchForm: $('.js-search-form'),
  searchFormInput: $('.js-search-input'),
  closeButton: $('.js-close-search'),
  menuNav: $('.js-menu-nav'),
  body: $('body')
}

const searchToggler = e => {
  e.preventDefault()
  e.stopPropagation()
  $el.search.toggleClass(css.activeClass)
  $el.body.toggleClass(css.blockedClass)
  setTimeout(() => {
    $el.searchFormInput.focus()
  }, 50)
}

const closeSearch = e => {
  e.preventDefault()
  $el.search.removeClass(css.activeClass)
  $el.body.removeClass(css.blockedClass)
}

const closeSearchOnBody = e => {
  if (!$(e.target).closest($el.searchForm).length) {
    $el.search.removeClass(css.activeClass)
    $el.body.removeClass(css.blockedClass)
  } else {
    e.stopPropagation()
  }
}

$el.headerSearchToggler.on('click', searchToggler)
$el.body.on('click', closeSearchOnBody)
$el.closeButton.on('click', closeSearch)
