import $ from 'jquery'
import ScrollMagic from 'scrollmagic/scrollmagic/minified/ScrollMagic.min.js'
import { isDesktop, convertToKebabCase } from './../utils'

// Debugging
// import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators' // Debugging

const linkHtml = props => {
  return `
    <li class="c-sticky-navigation__item">
      <a href="${props.href}" class="c-sticky-navigation__link">
          <span class="c-sticky-navigation__index">
            ${props.index}
          </span>
          <span class="c-sticky-navigation__title">
            ${props.text}
          </span>
      </a>
    </li>
  `
}

const controller = new ScrollMagic.Controller()
const mainContent = document.querySelector('.js-sticky-navigation-trigger')
const navigation = document.querySelector('.js-sticky-navigation-box')

function getDuration () {
  return mainContent.offsetHeight - navigation.offsetHeight
}

const stickyNavigation = {
  init: function () {
    if (!navigation) return
    if (isDesktop()) {
      $(document).on('ready', () => {
        this.createNavigation()
        this.createStickyEffect()
      })
    }
  },
  onLoadScrollToSec: function () {
    const currentHash = window.location.hash
    const elCurrentHash = $(currentHash)[0]
    if (!elCurrentHash) return

    this.scrollToSec(elCurrentHash, currentHash)
    window.location.hash = currentHash
  },
  scrollToSec: function (elCurrentHash, targetHash) {
    const offsetValue = $(elCurrentHash).offset().top

    $('html, body').animate({
      scrollTop: offsetValue
    }, 400, function () {
      window.location.hash = targetHash
    })
  },
  createStickyEffect: function () {
    new ScrollMagic.Scene({
      triggerElement: '.js-sticky-navigation-trigger',
      duration: getDuration(),
      triggerHook: 0 })
        .setPin('.js-sticky-navigation-box')
        .addTo(controller)
  },
  createNavigation: function () {
    $('h2').each(function (index, item) {
      const href = convertToKebabCase(item.innerText)
      const hrefHashed = `#${href}`
      const hrefSelector = `[href="${hrefHashed}"]`
      const currentLink = { index: index + 1, text: item.innerText, href: hrefHashed }

      const currrentSection = item.parentNode.parentNode
      currrentSection.id = convertToKebabCase(item.innerText)
      $('.js-sticky-navigation-list').append(linkHtml({...currentLink}))

      // build scene
      new ScrollMagic.Scene({
        triggerElement: hrefHashed,
        duration: currrentSection.offsetHeight
      })
        .setClassToggle(hrefSelector, 'is-active')
        // .addIndicators()
        .on('leave', function (e) {
          // eslint-disable-next-line
          history.replaceState(null, null, document.location.pathname)
        })
        .on('enter', function (e) {
          var newHash = href

          // eslint-disable-next-line
          history.replaceState(null, null, document.location.pathname + '#' + newHash)
        })
        .addTo(controller)

      $(hrefSelector).on('click', function (e) {
        e.preventDefault()
        const elHrefHashed = $(hrefHashed)
        stickyNavigation.scrollToSec(elHrefHashed, hrefHashed)
      })
    })

    $(document).on('ready', function () {
      stickyNavigation.onLoadScrollToSec()
    })
  }
}

stickyNavigation.init()
