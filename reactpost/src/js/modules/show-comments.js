import $ from 'jquery'

const $el = {
  commentTrigger: $('.js-comment-trigger'),
  commentsSec: $('.js-comments')
}

const showCommentSec = e => {
  e.preventDefault()
  e.stopPropagation()
  $el.commentsSec.slideDown('slow')
  $el.commentTrigger.hide()
}

$el.commentTrigger.on('click', showCommentSec)
