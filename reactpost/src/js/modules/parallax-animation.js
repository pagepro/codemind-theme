import $ from 'jquery'
import '../vendor/parallax'

const parallaxAnimation = {
  init: function () {
    const config = [
      ['#wykres', -1.5],
      ['#bg-shape', 2],
      ['#bg-shape-2', 2],
      ['#diamond-1', -2],
      ['#diamond-2', -1],
      ['#diamond-3', -3],
      ['#diamond-4', -2],
      ['#diamond-5', -2],
      ['#diamond-6', -4],
      ['#circle-2', -2],
      ['#circle-1', -2],
      ['#cross-1', -3],
      ['#cross-2', -2],
      ['#cross-3', -3],
      ['#line-1', -4],
      ['#line-2', -4],
      ['#arrow', 2],
      ['#Kształt_5', 2],
      ['#folder-1', -3],
      ['#folder-2', -2.6],
      ['#folder-3', -2.2],
      ['#folder-4', -1.8],
      ['#folder-5', -1.4],
      ['#react', -0.3],
      ['#tyl', -0.3],
      ['#bg-shape-1', -0.2],
      ['#hand-down', 0.7],
      ['#moneta-8', 0.7],
      ['#moneta-7', 0.7],
      ['#moneta-6', 0.7],
      ['#moneta-5', 0.7],
      ['#moneta-4', 0.7],
      ['#moneta-3', 0.7],
      ['#moneta-2', 0.7],
      ['#moneta-1', 0.7]
    ]

    config.forEach(function (item) {
      const currentEl = document.querySelector(item[0])
      if (currentEl) {
        currentEl.setAttribute('data-depth', item[1] / 2.2)
        currentEl.classList.add('layer')
      }
    })

    $(document).on('ready', function () {
      const parallaxScene = document.querySelector('.js-parallax-animation')
      if (!parallaxScene) return
      // eslint-disable-next-line
      new Parallax(parallaxScene)
    })
  }
}

const isExplorer = () => {
  function GetIEVersion () {
    var sAgent = window.navigator.userAgent
    var Idx = sAgent.indexOf('MSIE')

    if (Idx > 0) {
      return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf('.', Idx)))
      // eslint-disable-next-line
    } else if (!!navigator.userAgent.match(/Trident\/7\./)) {
      return 11
    } else {
      return 0
    }
  }

  if (GetIEVersion() > 0) {
    return true
  } else {
    return false
  }
}

if (!isExplorer()) {
  parallaxAnimation.init()
}
