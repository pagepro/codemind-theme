import $ from 'jquery'

const StickySocials = {
  init: function () {
    const $socials = $('.js-socials')
    const $window = $(window)
    const subHeroHeight = $('.js-sub-hero').height()

    $window.on('scroll', function () {
      const distance = $window.scrollTop()
      $socials.toggleClass('is-sticky', distance > subHeroHeight)
    })
  }
}
StickySocials.init()
