<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">
        <title>{% block title %}{{ title }}{% endblock %}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        {% include "head/head-links.tpl" %}
    </head>
    <body>

        {# {% import "partials/header.tpl" as l_header %}
        {{ l_header.render() }} #}


        {# imports #}
            {% from 'partials/temporary-components/hero.tpl' import hero %}
            {% from 'partials/temporary-components/chart.tpl' import chart %}
            {% from 'partials/temporary-components/podium.tpl' import podium %}
            {% from 'partials/temporary-components/takeaways.tpl' import takeaways %}
            {% from 'partials/temporary-components/pdf-download.tpl' import pdfDownload %}
            {% from 'partials/temporary-components/quote.tpl' import quote %}
            {% from 'partials/temporary-components/question-heading.tpl' import questionHeading %}
            {% from 'partials/temporary-components/share-social.tpl' import shareSocial %}
            {% from 'partials/temporary-components/sticky-navigation.tpl' import stickyNav %}
            {% from 'partials/temporary-components/decor-image.tpl' import decorImage %}
        {# / imports #}

        <!-- HERO COMPONENT - START -->
            {# Component Hero #}
                {{
                    hero({
                        heading: '<strong>React</strong><br> in a business perspective',
                        description: 'The report for C-level executives implementing  or planning to implement  React in their organizations.'
                    })
                }}
            {# / Component Hero #}
        <!-- HERO COMPONENT - END -->


        <!-- MAIN CONTENT - START -->

        <div class="l-container l-container--custom-blog-post js-sticky-navigation-trigger">
            {# Component stickyNav #}
                {{ stickyNav() }}
            {# Component stickyNav #}

            <div class="l-sec l-sec--custom-blog-post">
                <div class="l-inner ui-cms-custom-blog">
                    <h2>Introduction</h2>

                    {# Component decorImage #}
                        {{ decorImage({
                             src: './static/img/ilustration-1.png'
                        }) }}
                    {# Component decorImage #}

                    <h3>Why did we create this report?</h3>

                    <p>Finding a general knowledge about React is relatively easy.</p>
                    <p>You can use:</p>

                    <ul>
                        <li>Clear and precise documentation</li>
                        <li>Tons of blog articles and videos</li>
                        <li>Open source projects</li>
                        <li>Online courses</li>
                        <li>etc.</li>
                    </ul>

                    <p>The bigger problem is to find an exact top-level business knowledge about it. In other words, a knowledge about: </p>

                    <h3>How using React is actually impacting my business?</h3>

                    <p>React as any other piece of software is here to <strong>efficiently build web &amp; mobile products</strong>, but how is it actually impacting the modern world of development? </p>
                    <p>We know the number of downloads, number of repositories or number of open source projects, but what if these numbers are just vanity metrics and a true valuable business knowledge is somewhere hidden?</p>
                    <p>This report was made to help C-level executives decide <strong>if they should use React in their organizations or not</strong>.</p>

                    <p>It will also help organizations understand if using React is a good idea in a longer term and if they should keep this technology in their stack for the future. </p>

                    <h3>Who should read it?</h3>

                    <p>We made it for:</p>

                    <ul>
                        <li>Top-level tech leaders </li>
                        <li>Founders</li>
                        <li>Top-level Executives</li>
                        <li>Team Leaders</li>
                        <li>Senior developers</li>
                    </ul>

                    <p>That cares about efficiency in building web and mobile apps.</p>
                    <p>The truth is, each and every day technology is changing, and what seems to be a great idea one day, becomes outdated the other.</p>
                    <p>Developers became more demanding, and it is only becoming harder to find the one that will find a common language in the team.</p>
                    <p>This is why we spoke to people like <strong>CTOs with a need of clear understanding</strong> on how modern technology is able to fix common issues, make their work easier, and stay safe in the perspective of constant growth and rapid changes.</p>
                    <p>There are many <strong>C-level executives that are still hesitating</strong> if React is able to keep its promises, as to make appealing calculations of pros and cons appears to be extremely hard in such a technology rush.</p>
                    <p>There are also many <strong>top-level executives that may want to switch to React</strong> already, but still cannot find a good and convincing business reason to make the move.</p>
                    <p>There are hundreds of <strong>founders that would like to implement modern technology</strong>, and are still searching for the perfect one.</p>
                    <p>Also, <strong>companies working with React already</strong> will find great pieces of advice and insights from other top-level executives.</p>
                    <p>At the end, there are people that want, but are not sure how, or where to start <strong>without carrying a huge cost of baggage</strong>.</p>
                    <p>This report is for all of you.</p>
                </div>
            </div>

            {# Component pdfDownload #}
                {{
                    pdfDownload({
                        heading: 'The guide was delivered by Pagepro - React Rebels who can help you put your competition to shame by training, consulting, full product development.',
                        subheading: 'To get all this knowledge click the button below or read further!'
                    })
                }}
            {# / Component pdfDownload #}

            <div class="l-sec l-sec--custom-blog-post">
                <div class="l-inner ui-cms-custom-blog ui-animation-hook ui-animation-hook--up">
                    <h2>Market Context</h2>

                    {# Component decorImage #}
                        {{ decorImage({
                             src: './static/img/ilustration-2.png',
                             alt: true
                        }) }}
                    {# Component decorImage #}

                    <p>In 2020 most tech teams starting a new project are choosing one of three most popular solutions (React, Angular and Vue) for building their products. </p>
                    <p>Let’s start with checking if React is worth our interest by investigating some key market metrics.</p>


                    {# Component chart #}
                        {{
                            chart({
                                heading: 'React Popularity',
                                statisticsData: '
                                    [
                                        ["48,7%", "jQuery"],
                                        ["31,3%", "React", "active"],
                                        ["30,7%", "Angular/Angular.js"],
                                        ["26,3%", "ASP.NET"],
                                        ["19,7%", "Express"],
                                        ["16,2%", "Spring"],
                                        ["15,2%", "Vue.JS"],
                                        ["13,0%", "Django"],
                                        ["12,1%", "Flask"],
                                        ["10,5%", "Lavarel"],
                                        ["8,2%", "Ruby on rails"],
                                        ["3,5%", "Drupal"]
                                    ]
                                ',
                                source: 'https://insights.stackoverflow.com/survey/2019/#most-popular-technologies'
                            })
                        }}
                    {# / Component chart #}


                    {# Component podium #}
                        {{
                            podium({
                                isDecorated: true,
                                heading: 'React VS Competition:',
                                items: [
                                    {
                                        place: 2,
                                        name: 'Angular',
                                        value: '30.7%'
                                    },
                                    {
                                        place: 1,
                                        name: 'React',
                                        value: '33%'
                                    },
                                    {
                                        place: 3,
                                        name: 'Vue',
                                        value: '3.5%'
                                    }
                                ]
                            })
                        }}
                    {# / Component podium #}


                    {# Component chart #}
                        {{
                            chart({
                                heading: 'Which JavaScript frameworks are regularly used?',
                                desc: 'At the beginning of 2019, JetBrains polled almost 7,000 developers to identify the State of Developer Ecosystem. Let’s check how React was seen by the developers.',
                                statisticsData: '
                                    [
                                        ["54%", "React", "active"],
                                        ["40%", "Express"],
                                        ["39%", "Vue.js"],
                                        ["23%", "Angular/Angular.js"],
                                        ["20%", "React Native"],
                                        ["16%", "Electron"],
                                        ["14%", "AngularJS"],
                                        ["8%", "Cordova / PhoneGap"],
                                        ["3%", "Polymer"],
                                        ["3%", "Backbone"],
                                        ["2%", "Meteor"],
                                        ["2%", "Ember"],
                                        ["6%", "Other"],
                                        ["6%", "None"]
                                    ]
                                ',
                                source: 'https://www.jetbrains.com/lp/devecosystem-2019/javascript/'
                            })
                        }}
                    {# / Component chart #}


                    {# Component podium #}
                        {{
                            podium({
                                isDecorated: true,
                                heading: 'React VS Competition:',
                                items: [
                                    {
                                        place: 2,
                                        name: 'Vue',
                                        value: '39%'
                                    },
                                    {
                                        place: 1,
                                        name: 'React (74% with React Native)',
                                        value: '54%'
                                    },
                                    {
                                        place: 3,
                                        name: 'Angular',
                                        value: '14%'
                                    }
                                ]
                            })
                        }}
                    {# / Component podium #}




                    <h4>The need of React Developers</h4>
                    <p>Let’s take a look at the industry demand for specific developers by scanning Indeed job portal in different main development centers in Europe.</p>


                    <p>London, UK</p>
                    <ul>
                        <li>React: <strong>1,643 jobs</strong> <a target="_blank" rel="nofollow" href="https://www.indeed.co.uk/React-javascript-jobs-in-London">https://www.indeed.co.uk/React-javascript-jobs-in-London</a></li>
                        <li>Vue: <strong>286 jobs</strong> <a target="_blank" rel="nofollow" href="https://www.indeed.co.uk/Vue-javascript-jobs-in-London">https://www.indeed.co.uk/Vue-javascript-jobs-in-London</a></li>
                        <li>Angular: <strong>832 jobs</strong> <a target="_blank" rel="nofollow" href="https://www.indeed.co.uk/Angular-javascript-jobs-in-London">https://www.indeed.co.uk/Angular-javascript-jobs-in-London</a></li>
                    </ul>

                    <p>Paris, France</p>
                    <ul>
                        <li>React: <strong>759 jobs</strong> <a target="_blank" rel="nofollow" href="https://www.indeed.fr/Paris-Emplois-react-javascript">https://www.indeed.fr/Paris-Emplois-react-javascript</a></li>
                        <li>Vue: <strong>336 jobs</strong> <a target="_blank" rel="nofollow" href="https://www.indeed.fr/Paris-Emplois-vue-javascript">https://www.indeed.fr/Paris-Emplois-vue-javascript</a></li>
                        <li>Angular: <strong>785 jobs</strong> <a target="_blank" rel="nofollow" href="https://www.indeed.fr/Paris-Emplois-angular-javascript">https://www.indeed.fr/Paris-Emplois-angular-javascript</a></li>
                    </ul>

                    <p>Berlin, Germany</p>
                    <ul>
                        <li>React: <strong>634 jobs</strong> <a target="_blank" rel="nofollow" href="https://de.indeed.com/react-javascript-Jobs-in-Berlin">https://de.indeed.com/react-javascript-Jobs-in-Berlin</a></li>
                        <li>Vue: <strong>212 jobs</strong> <a target="_blank" rel="nofollow" href="https://de.indeed.com/vue-javascript-Jobs-in-Berlin">https://de.indeed.com/vue-javascript-Jobs-in-Berlin</a></li>
                        <li>Angular: <strong>356 jobs</strong> <a target="_blank" rel="nofollow" href="https://de.indeed.com/angular-javascript-Jobs-in-Berlin">https://de.indeed.com/angular-javascript-Jobs-in-Berlin</a></li>
                    </ul>

                    {# Component podium #}
                        {{
                            podium({
                                isDecorated: true,
                                heading: 'React VS Competition:',
                                items: [
                                    {
                                        place: 2,
                                        name: 'Angular',
                                        value: '1973'
                                    },
                                    {
                                        place: 1,
                                        name: 'React',
                                        value: '3036'
                                    },
                                    {
                                        place: 3,
                                        name: 'Vue',
                                        value: '834'
                                    }
                                ]
                            })
                        }}
                    {# / Component podium #}


                    {# Component chart #}
                        {{
                            chart({
                                heading: 'Developers’ satisfaction of using React',
                                desc: 'In the last year, Stack Overflow survey developers responded that React is the most loved web framework used in 2019.',
                                statisticsData: '
                                    [
                                        ["74,5%", "React.js", "active"],
                                        ["73,6%", "Vue.js"],
                                        ["68,3%", "Express"],
                                        ["65,6%", "Spring"],
                                        ["64,9%", "ASP.NET"],
                                        ["62,1%", "Django"],
                                        ["61,1%", "Flask"],
                                        ["60,1%", "Laravel"],
                                        ["57,6%", "Angular/Angular.js"],
                                        ["57,1%", "Ruby on Rails"],
                                        ["45,3%", "jQuery"],
                                        ["30,1%", "Drupal"]
                                    ]
                                ',
                                source: 'https://insights.stackoverflow.com/survey/2019/#most-loved-dreaded-and-wanted'
                            })
                        }}
                    {# / Component chart #}
                    {# Component podium #}
                        {{
                            podium({
                                isDecorated: true,
                                heading: 'React VS Competition:',
                                items: [
                                    {
                                        place: 2,
                                        name: 'Vue',
                                        value: '73,6%'
                                    },
                                    {
                                        place: 1,
                                        name: 'React',
                                        value: '75,5%'
                                    },
                                    {
                                        place: 3,
                                        name: 'Angular',
                                        value: '57,6%'
                                    }
                                ]
                            })
                        }}
                    {# / Component podium #}



                    <h4>New coming React developers on the market</h4>
                    <p>React is the framework most developers want to learn 32% of Hackerrank HackerRank Developer Skills Survey says it's the framework they're learning next.</p>
                    <p>That means we can expect more React developers on the market to fulfill the demand gap.</p>

                    {# Component chart #}
                        {{
                            chart({
                                graphLarge: true,
                                heading: 'Which frameworks do you plan on learning next?',
                                statisticsData: '
                                    [
                                        ["32,3%", "React", "active"],
                                        ["27,6%", "AngularJS"],
                                        ["26,3%", "Django"],
                                        ["23,6%", "Vue.js"],
                                        ["16,3%", "Ruby on Rails"],
                                        ["15,3%", "Spark"],
                                        ["15,1%", "Spring"],
                                        ["13,7%", "ExpressJS"],
                                        ["12,5%", ".NETCore"],
                                        ["11,0%", "Backbone.js"],
                                        ["8,9%", "ASP"],
                                        ["8,6%", "Ember"],
                                        ["8,4%", "Cocoa"],
                                        ["8,3%", "Struts"],
                                        ["8,1%", "Meteor"],
                                        ["8,0%", "Pyramid"],
                                        ["7,4%", "JSF"],
                                        ["7,1%", "Padrino"]
                                ]',
                                source: 'https://2019.stateofjs.com/front-end-frameworks/'
                            })
                        }}
                    {# / Component chart #}
                    {# Component podium #}
                        {{
                            podium({
                                isDecorated: true,
                                heading: 'React VS Competition:',
                                items: [
                                    {
                                        place: 2,
                                        name: 'Angular',
                                        value: '27,6%'
                                    },
                                    {
                                        place: 1,
                                        name: 'React',
                                        value: '32,3%'
                                    },
                                    {
                                        place: 3,
                                        name: 'Vue',
                                        value: '23,6%'
                                    }
                                ]
                            })
                        }}
                    {# / Component podium #}
                </div>
                {# Component takeaways #}
                    {{
                        takeaways({
                            heading: 'Market Context Key Takeaways',
                            items: [
                                'React is the most popular web & mobile development tool on the market. ',
                                'Developers like to work with React, companies are demanding React developers.',
                                'Vue is getting traction, but mostly for developer’s non commercial projects. Companies are not still convinced and not actively hiring people knowing Vue.',
                                'Angular has almost as great adoption rate as React, companies prefer to hire Angular developers than Vue, unfortunately, Angular developers are the less satisfied developers of their technology. '
                            ]
                        })
                    }}
                {# / Component takeaways #}
            </div>







            <div class="l-sec l-sec--custom-blog-post" style="background-color: #ffffff;">
                <div class="l-inner ui-cms-custom-blog ui-animation-hook ui-animation-hook--up">

                    <h2>About the research</h2>

                    {# Component decorImage #}
                        {{ decorImage({
                             src: './static/img/ilustration-4.png',
                             alt: true
                        }) }}
                    {# Component decorImage #}

                    <p>The research was made to help you uncover the advantages of using React from a business perspective.</p>
                    <p>We asked about 500 CTOs from:</p>
                    <ul>
                        <li>UK</li>
                        <li>France</li>
                        <li>Germany</li>
                        <li>Australia</li>
                        <li>Netherlands</li>
                    </ul>
                    <p>to share their experiences with React and React Native.</p>
                    <p>Our survey contained closed and open-ended questions that  were strictly related with business topics around software development.</p>
                    <p>After receiving the results we have also asked industry experts to share their thoughts and comments.</p>
                </div>
                <div class="l-sec l-sec--custom-blog-post" style="background-color: #E6E6E6;">
                    <div class="l-inner ui-cms-custom-blog">
                        <h3>Now, here we are! Super excited to share it all with you!</h3>
                    </div>
                </div>
            </div>




            <div class="l-sec l-sec--custom-blog-post">
                <div class="l-inner ui-cms-custom-blog ui-animation-hook ui-animation-hook--up">
                    <h2>Survey Results</h2>

                    {# Component decorImage #}
                        {{ decorImage({
                             src: './static/img/ilustration-3.png'
                        }) }}
                    {# Component decorImage #}

                    <p>Let’s take a look at how our respondents answered key business-oriented questions.  Under the answers, we also have some comments from industry experts.</p>

                    {# Component questionHeading #}
                        {{
                            questionHeading({
                                number: 1,
                                heading: 'Is React moving in the<br> right direction?'
                            })
                        }}
                    {# / Component questionHeading #}

                    {# Component chart #}
                        {{
                            chart({
                                graphSmall: true,
                                statisticsData: '
                                    [
                                        ["50%", "Agree", "active"],
                                        ["40%", "Agree strongly", "active"],
                                        ["30%", "Neutral"],
                                        ["20%", "Disagree"],
                                        ["10%", "Disagree strongly"]
                                    ]
                                '
                            })
                        }}
                    {# / Component chart #}

                    <h4>Why do you think React is moving in the right direction?</h4>

                    {# Component quote #}
                        {{
                            quote({
                                imgSrc: './static/img/avatar-tim-baker.jpg',
                                name: 'Tim Baker',
                                position: 'Managing Director',
                                company: 'Stamford Digital',
                                companyUrl: 'https://stamford.digital/',
                                quote: "
                                    <p>I agree that React is moving in the right direction. It has already helped transform many businesses, and has been adopted as the front-end technology of choice by many large tech companies such as Netflix.</p>
                                    <p>React components are easier to extend and maintain. These components help empower front-end development teams to increase productivity and save businesses time and money. In addition, React's reusable nature suits multivariate testing, and helps teams deliver better user experiences which have a positive knock-on effect to goal conversion rates.</p>
                                ",
                                isAlt: true
                            })
                        }}
                    {# / Component quote #}

                    {# Component quote #}
                        {{
                            quote({
                                imgSrc: './static/img/avatar-brandon-stiles.jpg',
                                name: 'Brandon Stiles',
                                position: 'Founder',
                                company: 'Fourloop.ai',
                                companyUrl: 'https://www.fourloop.ai/',
                                quote: '
                                    <p>React has been so modern and seems to have a good finger on the pulse of where modern web development is going.</p>
                                    <p>Using and developing their context API to take away some of the complexity that newer developers may experience while using something like Redux, and they keep updating.</p>
                                    <p>That’s why I think React is moving in the right direction. It seems to be taking the strengths and weaknesses of all types of programs, whether they are new or old, and coming up with solutions fairly often updating the library to match that.</p>
                                    <p>In terms of React and Gatsby, I think that it’s getting easier to create static sites versus things like WordPress. I’m seeing a lot of things like React Library and a really good-looking React templates come online so people are able to get a really nice website faster instead of having it totally coded from the beginning.</p>
                                '
                            })
                        }}
                    {# / Component quote #}

                    {# Component questionHeading #}
                        {{
                            questionHeading({
                                number: 2,
                                heading: 'For what types of projects are you mainly using React?'
                            })
                        }}
                    {# / Component questionHeading #}
                    {# Component chart #}
                        {{
                            chart({
                                graphSmall: true,
                                statisticsData: '
                                    [
                                        ["40%", "Consumer focused apps (B2C)", "active"],
                                        ["30%", "Busuness to business (B2B)", "active"],
                                        ["20%", "Employee and partner focused"],
                                        ["10%", "Other (please specify)"],
                                        ["0%", "Enterprise solutions"]
                                    ]
                                '
                            })
                        }}
                    {# / Component chart #}

                    <h4>In what kind of projects do you think React is the best tool to use and why?</h4>

                    {# Component quote #}
                        {{
                            quote({
                                imgSrc: './static/img/avatar-mariusz-marcak.jpg',
                                name: 'Mariusz Marcak',
                                position: 'Co-Founder',
                                company: 'JS Minds',
                                companyUrl: 'https://jsminds.com/',
                                quote: "
                                    <p>I personally believe that React fits for most of the general use cases. Our respondents are usually using React in business (B2C and B2B projects), but this is just the preference of our respondents, not the whole market. Our developers are applying it in their daily routine and I can see people using it more and more in their everyday life, as React can be easily used to speed up and improve many areas that we cope with.</p>
                                    <p>In general, the more we use web and mobile, the more we can use React to improve what we do.</p>
                                ",
                                isAlt: true
                            })
                        }}
                    {# / Component quote #}
                    {# Component quote #}
                        {{
                            quote({
                                imgSrc: './static/img/avatar-harris-robin-kalash.jpg',
                                name: 'Harris Robin Kalash',
                                position: 'Co-Founder & CTO',
                                company: 'Uplet',
                                companyUrl: 'https://www.uplet.app/',
                                quote: '
                                    <p>I would personally use react for all these cases, however the only thing i can think of for not using it in b2b is because react leaves more decisions up to you and is less opinionated than say Angular. Angular is probably better for enterprise because its very opinionated and comes with everything.</p>
                                    <p>Less decisions to make and more documented practices.</p>
                                '
                            })
                        }}
                    {# / Component quote #}













                    {# Component questionHeading #}
                        {{
                            questionHeading({
                                number: 3,
                                heading: 'Which of the following best describes the type of environment you work in?'
                            })
                        }}
                    {# / Component questionHeading #}
                    {# Component chart #}
                        {{
                            chart({
                                graphSmall: true,
                                statisticsData: '
                                    [
                                        ["62,0%", "Startup", "active"],
                                        ["19,0%", "An agency/software house", "active"],
                                        ["12,0%", "Enterrise app dev team"],
                                        ["6,5%", "Other (please specify)"]
                                    ]
                                '
                            })
                        }}
                    {# / Component chart #}
                    <h4>Why React can be a good tool to use for Startups?</h4>
                    {# Component quote #}
                        {{
                            quote({
                                name: 'Samuel Yang',
                                position: 'Founder',
                                company: 'Doppel',
                                companyUrl: 'https://getdoppel.com/',
                                quote: "
                                    <p>Doppel was the first app I ever built.  I had built some websites & web apps before, but never touched native code. So it was incredible to be able to start building components, buttons, screens, etc. right off the bat using just Javascript and React. With React Native, I could focus on building out features and native integrations, rather than spending my time figuring when a component was going to render, or what styling rules to apply.</p>
                                    <p>Oh and the best part is, with just one codebase for both the iOS and Android platforms, the build cycle is 2 times quicker than if I had to learn Swift and Kotlin.</p>
                                ",
                                isAlt: true
                            })
                        }}
                    {# / Component quote #}
                    {# Component quote #}
                        {{
                            quote({
                                imgSrc: './static/img/avatar-chris-lojniewski.jpg',
                                name: 'Chris Lojniewski',
                                position: 'CEO',
                                company: 'Pagepro',
                                companyUrl: 'https://pagepro.co/',
                                quote: '
                                    <p>As you will see in other answers, React allows you to speed up the development, faster time to market, and cut development costs. These are the main objectives of startups, right after a product-market fit. An incredibly huge network of React contributors work constantly on creating more and more possibilities to make React the ultimate answer in web development and make other developers life easier. Besides that, great thing about React is that you can reuse components you have already built, anywhere in your project, which makes the development process even easier and faster. Mind that you can find many great “ready-to-use” solutions, waiting to be applied into your project, absolutely free of charge.</p>
                                '
                            })
                        }}
                    {# / Component quote #}




                    {# Component questionHeading #}
                        {{
                            questionHeading({
                                number: 4,
                                heading: 'Is React boosting team productivity and facilitates further maintenance?'
                            })
                        }}
                    {# / Component questionHeading #}

                    {# Component chart #}
                        {{
                            chart({
                                graphSmall: true,
                                statisticsData: '
                                    [
                                        ["56%", "Agree", "active"],
                                        ["18%", "Agree Strongly", "active"],
                                        ["17%", "Neutral"],
                                        ["9%", "Disagree"],
                                        ["0%", "Disagree Strongly"]
                                    ]
                                '
                            })
                        }}
                    {# / Component chart #}

                    <h4>What makes React boost the productivity of your team?</h4>

                    {# Component quote #}
                        {{
                            quote({
                                imgSrc: './static/img/avatar-david-panart.jpg',
                                name: 'David Panart',
                                position: 'Founder',
                                company: 'I Will Code It',
                                companyUrl: '#',
                                quote: "
                                    <p>React is boosting the productivity of my team in two ways : <br>
                                    - Firstly, it's an awesomely well designed and maintained tool, making it very reliable and quickly improving / adapting to new web development patterns and needs<br>
                                    - Secondly, the size of its community makes it a great central library for frontend development arounch which many others add-ons / component libraries of higher grade are available and particularly well maintained too. Of course, it also makes hiring React devs easier, as 1 out of 4 hirings in web dev now requires being skilled with react.</p>
                                ",
                                isAlt: true
                            })
                        }}
                    {# / Component quote #}




                    {# Component questionHeading #}
                        {{
                            questionHeading({
                                number: 5,
                                heading: 'Is React making it faster to onboard new developers  to the project compared with other stacks?'
                            })
                        }}
                    {# / Component questionHeading #}
                    {# Component chart #}
                        {{
                            chart({
                                graphSmall: true,
                                statisticsData: '
                                    [
                                        ["37%", "Agree", "active"],
                                        ["37%", "Neutral"],
                                        ["13%", "Agree Strongly"],
                                        ["13%", "Disagree"],
                                        ["0%", "Disagree Strongly"]
                                    ]
                                '
                            })
                        }}
                    {# / Component chart #}
                    {# <h3>What feature of React makes it faster to onboard new developers?</h3> #}




                    {# Component questionHeading #}
                        {{
                            questionHeading({
                                number: 6,
                                heading: 'What is the most important factor when deciding on a mobile development approach?'
                            })
                        }}
                    {# / Component questionHeading #}
                    {# Component chart #}
                        {{
                            chart({
                                statisticsData: '
                                    [
                                        ["38%", "Cross-platform effficiency", "active"],
                                        ["25%", "Quality of user experience...", "active"],
                                        ["18%", "Cost to develop and..."],
                                        ["13%", "Skills match with the..."],
                                        ["6%", "Speed and time to market"],
                                        ["0%", "Other (please secify)"],
                                        ["0%", "Ease of updates and..."],
                                        ["0%", "Performance"]
                                    ]
                                '
                            })
                        }}
                    {# / Component chart #}

                    <h4>What are your common problems when developing cross-platform apps?</h4>


                    {# Component quote #}
                        {{
                            quote({
                                imgSrc: './static/img/avatar-charles-killer.jpg',
                                name: 'Charles Killer',
                                position: 'CTO',
                                company: 'App Sapiens',
                                companyUrl: 'https://appsapiens.uk/',
                                quote: "
                                    <p>The context to this question is that there is no such thing as native cross platform development, iOS uses a different language from Android. Over the years two main strategies have developed to write a single code base shared between both platforms.</p>

                                    <p>First is hybrid app development, where a website is wrapped in a webview. Hybrid app development has a bad reputation for being slow and having poor connectivity to the device's hardware.</p>

                                    <p>Second is where common elements for each platform are abstracted by another language. Examples include React Native and Flutter. This is the most popular way to develop a performant app in short time scales without a huge budget.</p>

                                    <p>At App Sapiens we trust React Native; the community is vast and it has been used in production by hundreds of apps over the years.</p>

                                    <p>React Native does, however, have its difficulties. If you have no experience with native app development then you will have to learn the quirks of both XCode and Android Studio in order to build and debug your apps.</p>

                                    <p>Fortunately there has been a recent development to ease this complexity: Expo. Expo is a further layer of abstraction on top of React Native which leverages the flexibility of JavaScript to bypass the build step for developers. Your JS is bundled up and sent to a native APK or IPA which they build for you. This leaves you to just write your JS (or TS) and focus on the functionality rather than fighting native dependency incompatibilities.</p>
                                ",
                                isAlt: true
                            })
                        }}
                    {# / Component quote #}

                    {# Component questionHeading #}
                        {{
                            questionHeading({
                                number: 7,
                                heading: 'What was the most important reason behind adding React to the technology stack?'
                            })
                        }}
                    {# / Component questionHeading #}
                    <ul>
                        <li>React is easy to learn and easy to use.</li>
                        <li>Performance and reusability. We were also interested in other frameworks but they really struggled to get real traction and get a bigger community around them. What eventually made our decision easier to start using it, was when Angular 2 was announced and there was quite a big difference between this and the previous version.</li>
                        <li>It enables the development of extensible and maintainable highly interactive JavaScript apps.</li>
                        <li>It was the technology of choice for the team that implemented our mobile app.</li>
                        <li>We have been building an enterprise-level website and app, we needed to be able to build and support the app in-house without native developers. The performance was also a high priority and we wanted to decouple the CMS from the front end so we chose React to render a headless site and included the new lazy and suspense features so that we could code split the payload and have total control over the rendering of the site on load.</li>
                        <li>Developer experience and happiness with it.</li>
                        <li>Development time, reducing the time it takes to build new features for our platform. </li>
                        <li>Code sharing between web, iOS, and Android. What used to be 3 distinct codebases is now basically one.</li>
                        <li>Cross-platform.</li>
                        <li>Choosing a stable and large developer ecosystem for our front-end development work. Team knowledge, market availability of talent. Decoupling front- and back-end development for better scalability and maintainability Time to market and ability to solve (most) bugs from a single code base. The platform was originally written in C# / Razor which was server-driven. We wanted to move more of the logic and processing to clients’ devices so we decided to go with React to replace server-side Razor rendering.</li>
                        <li>React Native is the best cross-platform mobile app solution at the moment in my opinion. It promises to be a scalable way of building large scale web applications. Bump in quality of experiences as well as no need to have multiple platform-native engineers waiting for another project for their platform. Sharing the engineers between all parts of the stack.</li>
                        <li>Component oriented approach.</li>
                        <li>The community that's currently working with it, making it the most convenient front-end library to work with today.</li>
                        <li>Ability to add advanced functionalities to the user experience (blocking navigation, asynchronous effects). Use techs that are more appreciated by the developers.</li>
                    </ul>
                </div>
                {# Component takeaways #}
                    {{
                        takeaways({
                            heading: 'Key Business Takeaways:',
                            items: [
                                'React is making time to market shorter and allows solving (most) bugs from a single code base, which makes software delivery more efficient.',
                                'React has a stable, large developer ecosystem and wide market of available talents.',
                                'Thanks to component oriented  approach and cross-platform features, React is an easy scalable way of building large-scale web & mobile apps.'
                            ]
                        })
                    }}
                {# / Component takeaways #}

                <div class="l-inner ui-cms-custom-blog ui-animation-hook ui-animation-hook--up">
                    {# Component questionHeading #}
                        {{
                            questionHeading({
                                number: 8,
                                heading: 'What were the doubts you had while adding React to your tech stack for the first time?'
                            })
                        }}
                    {# / Component questionHeading #}


                    <ul>
                        <li>Before trying React we were already using Backbone, Ember, and Angular. We were afraid that we might be spreading our skills too thin and we were not going to be actually experts in any of those technologies. Fair to say that from the above, React is the only one left in our stack.</li>
                        <li>It was a technology that none of the developers had experience with and it more came about due to necessity than anything else. We needed to develop a cross-platform application which was a new venture for us. We used Meteor with React to handle the cross-platform compiling of one codebase into a website and iOS/Android app.</li>
                        <li>Difficult to learn, adds a layer on top of native code. Often you end up having to know native fundamentals anyway.</li>
                        <li>Being able to integrate CMS specific features with React. i.e. we used Episerver which has a live on-page edit feature, this feature is an iframe that reloads every time an update is made. This was difficult to figure out how to send and receive updates when changes had been made and then re-render the react app.</li>
                        <li>Back in 2016, there wasn't much talent out there with production experience. Now things are a lot better.</li>
                        <li>Dependency management across platforms.</li>
                        <li>A massive change in the approach meant that there is a risk that we're spending all the time trying to skill up for nothing. However, it turned out it was time well spent.</li>
                        <li>Is it stable enough? </li>
                    </ul>
                </div>

                {# Component takeaways #}
                    {{
                        takeaways({
                            heading: 'Key Business Takeaways:',
                            subheading: 'While implementing new technology to the company’s stack it’s important to check if:',
                            items: [
                                'This particular technology is stable enough.',
                                'Is it able to fulfill your company needs?',
                                'How to train your current dev team or hire new people with a solid commercial experience?'
                            ]
                        })
                    }}
                {# / Component takeaways #}

                <div class="l-inner ui-cms-custom-blog ui-animation-hook ui-animation-hook--up">
                    {# Component questionHeading #}
                        {{
                            questionHeading({
                                number: 9,
                                heading: 'What are the biggest advantages that React brings to your organization?'
                            })
                        }}
                    {# / Component questionHeading #}
                    <ul>
                        <li>Reusable components have benefited several recent projects that we've been involved in.</li>
                        <li>Ecosystem, dev tools, new frameworks based on React are amazing. There's a great community around it. Thanks to that our job is more efficient, products are more performant and at the end that results with happy clients.</li>
                        <li>It allows our developers to work with a small toolset and fundamental, modern JavaScript features and methodologies. It's easy for a new developer to pick up once they understand the concepts but still works well for more complex features of an app </li>
                        <li>Single codebase.</li>
                        <li>From a front end perspective, we have much more control over the performance and perceived loading times of any React site. It also allows us to add iOS and Android apps to our offering as we can now use React Native in-house. </li>
                        <li>Our web portals are very high quality in terms of visible user bugs.</li>
                        <li>Agility and ability to build new MVP features fast.</li>
                        <li>Increased speed and decreased cost. A single team can manage multiple apps on different platforms.</li>
                        <li>Having one way of building front-ends, across mobile apps and web apps.</li>
                        <li>Common structure and understanding of code base.</li>
                        <li>Cross-platform re-use (web and app), maintainability and well-performing front-ends (rich and fast UX).</li>
                        <li>Development speed and the ability to launch on Android and iOS.</li>
                        <li>Ability to centralize UI components into a single NPM shareable across multiple apps, thus providing the same look & feel to all apps.</li>
                        <li>It is a framework that allows our small team to improve our web app and mobile app development skills at the same time. </li>
                        <li>Speed to market and ability to deliver on desired customer experience. </li>
                        <li>Shareable library of inhouse components, ease of integration of other open source components.</li>
                    </ul>
                </div>
                {# Component takeaways #}
                    {{
                        takeaways({
                            heading: 'Key Business Takeaways:',
                            subheading: 'While implementing new technology to the company’s stack it’s important to check if:',
                            items: [
                                'After implementing React, organizations can increase development  speed and decrease development cost.  ',
                                'Thanks to React, a single team can manage multiple apps on different platforms.',
                                'React allows companies to build a shareable, reusable library of inhouse components that can be used in different projects, prototypes, and platforms.'
                            ]
                        })
                    }}
                {# / Component takeaways #}

                <div class="l-inner ui-cms-custom-blog ui-animation-hook ui-animation-hook--up">
                    {# Component questionHeading #}
                        {{
                            questionHeading({
                                number: 10,
                                heading: "What's still missing in React in your opinion?"
                            })
                        }}
                    {# / Component questionHeading #}
                    <ul>
                        <li>Resources on CMS integration. We know we are going to come up against new challenges when we attempt to integrate Sitecore with a headless site.</li>
                        <li>Certain libraries.</li>
                        <li>Pre-built components.</li>
                        <li>A good form builder.</li>
                        <li>Tooling around it. From SSR frameworks like NextJs to testing frameworks to CSS management to bootstrapping boilerplate, most of the existing tools seem to be work in progress. It is getting there though.</li>
                        <li>Solid testing approach and community consensus on the best approach to common problems. There is a wide spectrum of approaches available to handle state management e.g. I would love to be able to create platform-specific SDK from React code.</li>
                    </ul>
                </div>

                {# Component takeaways #}
                    {{
                        takeaways({
                            heading: 'Key Business Takeaways:',
                            subheading: "Respondents haven’t mentioned a big number of things missing in React, but they're still missing some libraries and strategies for resolving their software needs."
                        })
                    }}
                {# / Component takeaways #}

                <div class="l-inner ui-cms-custom-blog ui-animation-hook ui-animation-hook--up">
                    {# Component questionHeading #}
                        {{
                            questionHeading({
                                number: 11,
                                heading: "Would you like to use React for your next project?"
                            })
                        }}
                    {# / Component questionHeading #}

                    {# Component chart #}
                        {{
                            chart({
                                graphSmall: true,
                                statisticsData: '
                                    [
                                        ["57%", "Definitely yes", "active"],
                                        ["31%", "Yes", "active"],
                                        ["6%", "Neutral"],
                                        ["6%", "No"],
                                        ["0%", "Definitely no"]
                                    ]
                                '
                            })
                        }}
                    {# / Component chart #}

                    {# Component questionHeading #}
                        {{
                            questionHeading({
                                number: 12,
                                heading: "Do you think the number of employees using React in your organization will increase in the next 12 months?"
                            })
                        }}
                    {# / Component questionHeading #}

                    {# Component chart #}
                        {{
                            chart({
                                graphSmall: true,
                                statisticsData: '
                                    [
                                        ["57%", "Yes", "active"],
                                        ["31%", "Definitely yes", "active"],
                                        ["12%", "Neutral"],
                                        ["0%", "No"],
                                        ["0%", "Definitely no"]
                                    ]
                                '
                            })
                        }}
                    {# / Component chart #}

                    {# Component questionHeading #}
                        {{
                            questionHeading({
                                number: 13,
                                heading: "What is the strongest alternative to React and why?"
                            })
                        }}
                    {# / Component questionHeading #}
                    <ul>
                        <li>From a personal perspective, I'd probably go Vue, from a business perspective we would be interested in the latest Angular (whichever it is now ;) ) - this is the one framework that was specifically requested by a few of our potential clients.</li>
                        <li>I think that Vue is the strongest alternative to React because it offers a similar USP (a reactive DOM, allows creating a SPA, etc.) and allows the writing of more expressive and declarative code than something like vanilla JS or jQuery would. </li>
                        <li>We run projects in Vue also and this is good for smaller function-specific apps. The eco-system is very strong in Vue and the updates are becoming more in-sync with the way React is going. Both are great tools. </li>
                        <li>Vue.js, learning curve is less. </li>
                        <li>Vue potentially and maybe flutter. Not convinced there is a combo out there that can beat React/React-Native. </li>
                        <li>Probably Vue, and Flutter for App dev is interesting. </li>
                        <li>We are also looking into Vue.js for websites with less complexity, that only require a few interactions - for those sites the footprint of Vue is smaller and suited better, but if we have more complex projects with the possibility to go cross-device, we'd still opt for React </li>
                        <li>Native development, because it offers more control and allows you to easily create platform-specific SDK's directly </li>
                        <li>Angular because it gives developers a more structured approach that some people prefer. The instant you start a new angular project you have a lot more tools (e.g. Typescript) and structure to work with. </li>
                    </ul>

                    {# Component quote #}
                        {{
                            quote({
                                imgSrc: './static/img/avatar-pete-heslop.jpg',
                                name: 'Pete Heslop',
                                position: 'Managing Director',
                                company: 'Steadfast Collective',
                                companyUrl: 'https://steadfastcollective.com/',
                                quote: "
                                    <p>We decided to use Vue instead of React, because the learning curve and documentation made on boarding everyone easier. It fits nicely with our Laravel stack and ecosystem.</p>
                                ",
                                isAlt: true
                            })
                        }}
                    {# / Component quote #}
                </div>

                {# Component takeaways #}
                    {{
                        takeaways({
                            heading: 'Key Business Takeaways:',
                            subheading: "In most cases Vue is mentioned as the strongest React alternative,  especially for smaller projects. Enterprise size companies rather want to pick Angular instead of Vue."
                        })
                    }}
                {# / Component takeaways #}

                <div class="l-inner ui-cms-custom-blog ui-animation-hook ui-animation-hook--up">
                    {# Component questionHeading #}
                        {{
                            questionHeading({
                                number: 14,
                                heading: "When it comes to finding/hiring the right React developer, how would you characterize the current situation?"
                            })
                        }}
                    {# / Component questionHeading #}

                    {# Component chart #}
                        {{
                            chart({
                                graphSmall: true,
                                statisticsData: '
                                    [
                                        ["74%", "A moderate challange", "active"],
                                        ["26%", "A significant challange"],
                                        ["0%", "Not a challange at..."]
                                    ]
                                '
                            })
                        }}
                    {# / Component chart #}

                    <h4>Is hiring a React developer harder than one year before? Do you think that it will be easier next year?</h4>



                    {# Component quote #}
                        {{
                            quote({
                                imgSrc: './static/img/avatar-owen-clements.jpg',
                                name: 'Owen Clements',
                                position: 'Associate Javascript Consultant',
                                company: 'RJC Group Ltd',
                                companyUrl: 'http://www.rjcgroup.co.uk/',
                                quote: "
                                    <p>Since around a year ago I would say it is easier to find the right React developer. Obviously times such as Christmas do make it harder but, there is always quality React developers I can get hold of that will be placed.</p>
                                    <p>I also see a huge spike in the number of users of React and decrease of Angular users.</p>
                                ",
                                isAlt: true
                            })
                        }}
                    {# / Component quote #}


                    {# Component quote #}
                        {{
                            quote({
                                imgSrc: './static/img/avatar-george-maksimenko.jpg',
                                name: 'George Maksimenko',
                                position: 'Head of Business Development',
                                company: 'Adexin',
                                companyUrl: 'https://adexin.com/',
                                quote: "
                                    <p>We find the Finnish IT market very technological and modern. Due to a small domestic market many startups and companies think globally and internationally. Access to investments makes it possible to test new ideas and bring new services and products to their customers quickly. We learned that many companies prefer to work with local IT service providers, and without help, it can be difficult for offshore companies to earn trust and get into the Finnish IT market. The overall experience working with Finns is very good, and we got to work with some great people.</p>
                                "
                            })
                        }}
                    {# / Component quote #}
                </div>
            </div>


            <div class="l-sec l-sec--custom-blog-post">
                <div class="l-inner ui-cms-custom-blog ui-animation-hook ui-animation-hook--up">
                    <h2>More Resources</h2>

                    {# Component decorImage #}
                        {{ decorImage({
                             src: './static/img/ilustration-5.png'
                        }) }}
                    {# Component decorImage #}

                    <h4>More resources about the business impact of React on tech organizations</h4>
                    <ul>
                        <li><a target="_blank" rel="nofollow" href="https://pagepro.co/blog/2020/01/16/how-react-native-can-cut-your-development-costs/">How React Native Can Cut Your Development Costs</a></li>
                        <li><a target="_blank" rel="nofollow" href="https://pagepro.co/blog/2020/02/12/when-does-it-make-sense-to-use-react-native">When Does It Make Sense To Use React Native?</a></li>
                    </ul>

                    <h4>More resources about the business impact of React on tech organizations</h4>
                    <ul>
                        <li><a target="_blank" rel="nofollow" href="https://insights.stackoverflow.com/survey/2019/#overview">Stack Overflow’s annual Developer Survey</a></li>
                        <li><a target="_blank" rel="nofollow" href="https://2019.stateofjs.com/">State of JS 2019</a></li>
                        <li><a target="_blank" rel="nofollow" href="https://gist.github.com/tkrotoff/b1caa4c3a185629299ec234d2314e190">Front end frameworks popularity (React, Vue and Angular)</a></li>
                        <li><a target="_blank" rel="nofollow" href="https://research.hackerrank.com/developer-skills/2020">HackerRank Developer Skills Report 2020</a></li>
                        <li><a target="_blank" rel="nofollow" href="https://www.jetbrains.com/lp/devecosystem-2019/">The State of Developer Ecosystem 2019</a></li>
                    </ul>

                    <h4>Materials</h4>
                    <ul>
                        <li>Illustrations <a target="_blank" rel="nofollow" href="https://undraw.co/">https://undraw.co/</a></li>
                    </ul>
                </div>
            </div>


            {# Component shareSocial #}
                {{
                    shareSocial({
                        heading: 'Don’t forget to share this post',
                        socials: [
                            {
                                type: 'facebook',
                                icon: 'facebook',
                                href: 'https://www.facebook.com/thisispagepro/'
                            },
                            {
                                type: 'twitter',
                                icon: 'twitter',
                                href: 'https://twitter.com/pagepro_agency/'
                            },
                            {
                                type: 'linkedin',
                                icon: 'linkedin',
                                href: 'https://www.linkedin.com/company/pagepro/'
                            }
                        ]
                    })
                }}
            {# / Component shareSocial #}

        </div>


        <!-- MAIN CONTENT - END -->


        {% include "partials/footer.tpl" %}
        <script src="./static/js/app.js"></script>
    </body>
</html>
