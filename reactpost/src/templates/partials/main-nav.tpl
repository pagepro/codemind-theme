<nav class="c-main-nav" aria-label="main navigation">
    <div class="c-main-nav__toggler">
        <a href="#" class="c-menu-close js-menu-close" title="Close">
            <span class="c-menu-close__inner">
                <svg class="o-icon o-icon--close">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#close"></use>
                </svg>
            </span>
        </a>
    </div>
    <div class="c-main-nav__navigation">
        <ul class="c-main-nav-list">
            <li class="c-main-nav-list__item {% if _active_no == 0 %}is-active{% endif %}">
                <a href="{{ logo_href }}" class="c-main-nav-list__item__link theme-color-2 theme-l-height-1">
                    <span class="theme-font-1 theme-size-8 theme-weight-2">Home</span>
                </a>
            </li>
            <li class="c-main-nav-list__item {% if _active_no == 1 %}is-active{% endif %}">
                <a href="style-guide.html" class="c-main-nav-list__item__link theme-color-2 theme-l-height-1">
                    <span class="theme-font-1 theme-size-8 theme-weight-2">Archive</span>
                </a>
            </li>
            <li class="c-main-nav-list__item {% if _active_no == 2 %}is-active{% endif %}">
                <a href="#" class="c-main-nav-list__item__link theme-color-2 theme-l-height-1">
                    <span class="theme-font-1 theme-size-8 theme-weight-2">Contact</span>
                </a>
            </li>
        </ul>
        <div class="c-categories-nav">
            <p class="c-categories-nav__title theme-color-2 t-upper theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4">Categories:</p>
            <ul class="c-categories-nav-list">
                <li class="c-categories-nav-list__item {% if _active_no == 3 %}is-active{% endif %}">
                    <a href="#" class="c-categories-nav-list__item__link theme-color-2 theme-size-4 theme-l-height-1">
                        <span class="theme-font-1 theme-weight-1">
                            Typography
                        </span>
                    </a>
                </li>
                <li class="c-categories-nav-list__item {% if _active_no == 4 %}is-active{% endif %}">
                    <a href="#" class="c-categories-nav-list__item__link theme-color-2 theme-size-4 theme-l-height-1">
                        <span class="theme-font-1 theme-weight-1">
                            WordPress
                        </span>
                    </a>
                </li>
                <li class="c-categories-nav-list__item {% if _active_no == 5 %}is-active{% endif %}">
                    <a href="#" class="c-categories-nav-list__item__link theme-color-2 theme-size-4 theme-l-height-1">
                        <span class="theme-font-1 theme-weight-1">
                            Photography
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="c-main-nav__socials">
        <p class="c-simple-text theme-color-6 theme-font-1 theme-size-4 theme-style-2 theme-weight-2 theme-l-height-2">
            Stay in touch
        </p>
        <ul class="c-socials-list">
            <li class="c-socials-list__item">
                <a href="#" class="c-socials-list__item__link">
                    <svg class="o-icon o-icon--twitter">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#twitter"></use>
                    </svg>
                </a>
            </li>
            <li class="c-socials-list__item">
                <a href="#" class="c-socials-list__item__link">
                    <svg class="o-icon o-icon--facebook">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#facebook"></use>
                    </svg>
                </a>
            </li>
            <li class="c-socials-list__item">
                <a href="#" class="c-socials-list__item__link">
                    <svg class="o-icon o-icon--pinterest">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#pinterest"></use>
                    </svg>
                </a>
            </li>
            <li class="c-socials-list__item">
                <a href="#" class="c-socials-list__item__link">
                    <svg class="o-icon o-icon--instagram">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#instagram"></use>
                    </svg>
                </a>
            </li>
            <li class="c-socials-list__item">
                <a href="#" class="c-socials-list__item__link">
                    <svg class="o-icon o-icon--snapchat">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#snapchat"></use>
                    </svg>
                </a>
            </li>
        </ul>
    </div>
</nav>