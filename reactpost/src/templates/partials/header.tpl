{% macro render(_active_no=false, logo_href) %}
    {% if _active_no == '' %}
        {% set logo_href='#' %}
    {% else %}
        {% set logo_href='index.html' %}
    {% endif %}
    <header class="l-header js-sticky-header">
        <div class="l-header__inner">
            <div class="l-header__box">
                <div class="l-header__menu-toggler">
                    <a href="#" class="c-menu-toggler js-menu-toggler">
                        <span class="c-menu-toggler__wrap">
                            <span class="c-menu-toggler__lines"></span>
                            <span class="c-menu-toggler__info">Menu</span>
                        </span>
                    </a>
                </div>
                <div class="l-header__title">
                    <h1 class="c-logo c-logo--main">
                    <a href="{{ logo_href }}" class="c-logo__inner">
                        <img src="./static/img/logo.png" alt="Code Mind">
                        <span class="c-label">Code Mind</span>
                    </a>
                    </h1>
                    <p class="c-subtitle theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4 t-upper">probably the best blog theme for developer</p>
                </div>
                <div class="l-header__tools">
                    <div class="c-search">
                         <a href="#" class="c-search__toggler c-btn c-btn--icon js-search-toggler">
                            <span class="c-btn__content">
                                <svg class="o-icon o-icon--search">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#search"></use>
                                </svg>
                                <span class="c-label">Search</span>
                            </span>
                        </a>
                        <div class="c-search__wrapper theme-bg-1 js-search">
                            <a href="#" class="c-search__close js-close-search" title="Close search">
                                <svg class="o-icon o-icon--close">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#close"></use>
                                </svg>
                            </a>
                            <form class="f-search-box t-center js-search-form">
                                <div class="f-search-box__inner">
                                    <input name="search" type="text" class="f-search-box__input theme-font-1 theme-size-10 theme-l-height-3 t-weight-1 theme-color-2 js-search-input" placeholder="Search">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="l-header__box">
                <div class="l-header__nav-container js-menu-nav">
                    <div class="l-header__nav">
                        {% include "partials/main-nav.tpl" %}
                    </div>
                </div>
            </div>
        </div>
    </header>
{% endmacro %}
