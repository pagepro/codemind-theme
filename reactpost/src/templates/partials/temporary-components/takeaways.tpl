{% macro takeaways(item) %}
    <div class="c-takeaways-wrapper">
        <div class="c-takeaways ui-animation-hook ui-animation-hook--up">
            <div class="c-takeaways__inner l-inner">
                <h3 class="c-takeaways__heading">
                    {{ item.heading | safe }}
                </h3>
                {% if item.subheading %}
                    <div class="c-takeaways__subheading">
                        {{ item.subheading | safe }}
                    </div>
                {% endif %}
                <ul class="c-takeaways__list">
                    {% for singleItem in item.items %}
                    <li class="c-takeaways__item">
                        {{ singleItem | safe }}
                    </li>
                    {% endfor %}
                </ul>
            </div>
        </div>
    </div>
{% endmacro %}
