{% macro pdfDownload(item) %}
    <div class="c-pdf-download-wrapper">
        <div class="l-inner c-pdf-download">
            <div data-trigger-hook="0.8" class="c-pdf-download__inner ui-animation-hook ui-animation-hook--up">
                <div class="c-pdf-download__content">
                    <h3 class="c-pdf-download__heading">
                        {{ item.heading | safe }}
                    </h3>
                    <p class="c-pdf-download__subheading">
                        {{ item.subheading | safe }}
                    </p>
                </div>
                <form class="c-pdf-download__form">
                    <div class="c-pdf-download__field">
                        <input class="c-pdf-download__input" type="email" name="email" value="" placeholder="E-mail Address">
                        <button type="submit" class="c-pdf-download__btn">Send PDF</button>
                    </div>
                </form>
                <p class="c-pdf-download__policy">Pagepro is the sole administrator of the data provided to us by third parties. By downloading our resources or sending us inquiries, you grant us permission to send you further correspondence, such as our company newsletter, event notifications, or updates on the services we provide. You may unsubscribe from our messaging or change the preferences of what you receive from us at any time. Read our <a href="privacy.html" target="_blank">Privacy Policy</a> to learn more about the ways we protect and secure your personal information.</p>
            </div>
        </div>
    </div>
{% endmacro %}
