{% macro hero(item) %}
    <div class="c-hero-wrapper">
        <div class="c-hero">
            <div class="l-inner c-hero__inner">
                <div class="c-hero__content ui-animation-hook ui-animation-hook--up">
                    <h1 class="c-hero__title">
                        {{ item.heading | safe }}
                    </h1>
                    <h3 class="c-hero__desc ">
                        {{ item.description | safe }}
                    </h3>
                </div>
            </div>
            <div class="c-hero__overlay">
                <div class="c-hero__overlay__media">
                    <div class="c-hero__overlay__img">
                        <div class="c-hero-animation js-parallax-animation" style="width: 100%; max-width: 1000px;">
                            {% include "partials/temporary-components/hero-animated.tpl" %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endmacro %}
