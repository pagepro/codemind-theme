{% macro podium(item) %}
    <div data-trigger-hook="0.8" class="c-podium-wrapper ui-animation-hook c-block-decorated-wrapper">
        <div class="c-podium c-block-decorated">
            <div class="c-podium__inner">
                <h4 class="c-podium__heading">
                    {{ item.heading | safe }}
                </h4>
                <div class="c-podium__list">
                    {% for podium in item.items %}
                    <div class="c-podium__item">
                        <div class="c-podium-place c-podium-place--{{ podium.place }}">
                            {% if podium.place == 1 %}
                                <figure class="c-podium-place__cup-wrapper">
                                    <img class="c-podium-place__cup" alt="Trophy ilustration" src="./static/img/cup.svg"/>
                                </figure>
                            {% else %}
                                <span class="c-podium-place__number">{{ podium.place }}</span>
                            {% endif %}
                            <span class="c-podium-place__title"><strong>{{ podium.name }}</strong> ({{ podium.value }})</span>
                        </div>
                    </div>
                    {% endfor %}
                </div>
            </div>
        </div>
    </div>
{% endmacro %}
