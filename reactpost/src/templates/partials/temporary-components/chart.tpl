{% macro chart(item) %}
    <div class="c-chart-wrapper">
        <div class="c-chart">
            {% if item.heading or item.desc %}
                <div class="c-chart__inner">
                    <div class="c-chart__content">
                        {% if item.heading %}
                            <h3 class="c-chart__title">
                                {{ item.heading | safe }}
                            </h3>
                        {% endif %}
                        {% if item.desc %}
                        <p class="c-chart__desc">
                            {{ item.desc | safe }}
                        </p>
                        {% endif %}
                    </div>
                </div>
            {% endif %}

            <div data-trigger-hook="0.8" class="c-chart-graph-wrapper {% if item.graphSmall %} c-chart-graph-wrapper-small {% endif %} {% if item.graphLarge %} c-chart-graph-wrapper-large {% endif %} js-chart-graph" data-statistics="{{ item.statisticsData }}">
                <canvas class="c-chart-graph" style="display: block; width: 100%; height: 100%;"></canvas>
            </div>

            {% if item.source %}
                <div class="c-chart__inner">
                    <div class="c-chart__source">
                        Source: <a href="{{ item.source }}" rel="nofollow" class="c-chart__source-link" target="_blank">{{ item.source }}</a>
                    </div>
                </div>
            {% endif %}
        </div>
    </div>
{% endmacro %}
