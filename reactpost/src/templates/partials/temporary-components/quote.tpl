{% macro quote(data) %}
    <div class="c-quote-wrapper ui-animation-hook">
        <div class="c-quote {% if data.isAlt %} c-quote--alt {% endif %}">
            <div class="c-quote__inner">
                <blockquote class="c-quote__desc">
                    {{ data.quote | safe }}
                </blockquote>
                <div class="c-quote__author">
                    {% if data.imgSrc %}
                        <div class="c-quote__avatar">
                            <img class="c-quote__img" src="{{ data.imgSrc }}" alt="Profile picture of author {{ data.name }}">
                        </div>
                    {% endif %}
                    <div class="c-quote__name">
                        {{ data.name | safe }}
                    </div>
                    <div class="c-quote__position">
                        {{ data.position | safe }}
                    </div>
                    <a href="{{ data.companyUrl }}" rel="nofollow" target="_blank" class="c-quote__company">
                        {{ data.company | safe }}
                    </a>
                </div>
            </div>
        </div>
    </div>
{% endmacro %}
