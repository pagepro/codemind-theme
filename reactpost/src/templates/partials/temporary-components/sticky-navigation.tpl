{% macro stickyNav() %}
    <div class="c-sticky-navigation-wrapper js-sticky-navigation-box">
        <div class="c-sticky-navigation ui-animation-hook ui-animation-hook--left">
            <div class="c-sticky-navigation__inner">
                <ul class="c-sticky-navigation__list js-sticky-navigation-list">
                </ul>
            </div>
        </div>
    </div>
{% endmacro %}
