
{% macro shareSocial(data) %}
    <div class="c-share-social-wrapper">
        <div class="c-share-social l-inner">
            <div class="c-share-social__inner">
                <h3 class="c-share-social__heading">
                    {{ data.heading | safe }}
                </h3>
                <ul class="c-share-social__list">
                    {% for social in data.socials %}
                        <li class="c-share-social__item">
                            <a href="{{ social.href }}" rel="nofollow" target="_blank" class="c-share-social__item__link">
                                <svg class="o-icon o-icon--{{ social.type }}">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#{{ social.icon }}"></use>
                                </svg>
                            </a>
                        </li>
                    {% endfor%}
                </ul>
            </div>
        </div>
    </div>
{% endmacro %}
