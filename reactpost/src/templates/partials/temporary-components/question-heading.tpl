{% macro questionHeading(item) %}
    <div class="c-question-heading-wrapper">
        <div class="c-question-heading">
            <div class="c-question-heading__inner">
                <div class="c-question-heading__number">
                    Q{{ item.number | safe }}
                </div>
                <h3 class="c-question-heading__heading">
                    {{ item.heading | safe }}
                </h3>
            </div>
        </div>
    </div>
{% endmacro %}
