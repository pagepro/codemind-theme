{% macro decorImage(item) %}
    <div class="c-decor-image-wrapper">
        <img src="{{ item.src }}" alt="Ilustration" class="c-decor-image {% if item.alt %} c-decor-image--right {% endif %}"/>
    </div>
{% endmacro %}
