<footer class="l-footer">
    <div class="l-footer__inner">
        <div class="c-copyright theme-font-1 theme-size-2 theme-weight-1 theme-l-height-3">
            <span>&copy; 2017 by</span>
            <a href="#">{{ author }}</a>
        </div>

        <div class="c-partners-wrapper">
            <div class="c-partners">
                <div class="c-partners__title">
                    Partners:
                </div>
                <div class="c-partners__list">
                    <li class="c-partners__item">
                        <a class="c-partners__link">
                            <figure class="c-partners__media">
                                <img class="c-partners__img" src="./static/img/img-partner-1.png" alt=""/>
                            </figure>
                        </a>
                    </li>
                    <li class="c-partners__item">
                        <a class="c-partners__link">
                            <figure class="c-partners__media">
                                <img class="c-partners__img" src="./static/img/img-partner-2.png" alt=""/>
                            </figure>
                        </a>
                    </li>
                </div>
            </div>
        </div>

        <div class="c-newsletter-teaser">
            <span class="c-newsletter-teaser__info theme-font-1 theme-size-2 theme-weight-1 theme-l-height-3">Want to receive weekly updates?</span>
            <span class="c-newsletter-teaser__info theme-font-1 theme-size-2 theme-weight-1 theme-l-height-3">
                <a href="#" class="c-newsletter-teaser__link theme-color-3 js-open-popup">
                Sign up to the newsletter</a>
                <span>and never miss new article.</span>
            </span>
        </div>
        <div class="c-popup c-popup--newsletter js-newsletter-popup theme-bg-3">
            <div class="c-popup__inner">
                <div class="c-popup__content">
                    <form class="f-form c-newsletter">
                        <div class="c-newsletter__header">
                            <p class="theme-color-2 theme-font-1 theme-size-4 theme-weight-2 theme-l-height-2">
                                We have two really good reasons to remove toxins from devices. People and the planet.
                            </p>
                        </div>
                        <div class="c-newsletter__action">
                            <div class="c-action-box c-action-box--wide">
                                <input class="c-action-box__input theme-font-1 theme-size-3 theme-weight-1 theme-l-height-2 theme-bg-2 theme-color-6" placeholder="Your email here">
                                <button class="c-action-box__btn c-btn theme-bg-2">
                                    <span class="c-btn btn__content">
                                        <span class="c-label theme-color-3 t-upper theme-font-1 theme-size-1 theme-weight-2 theme-l-height-2">Subscribe</span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <a href="#" class="c-popup__close js-close-popup" title="Close popup">
                    <svg class="o-icon o-icon--close">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#close"></use>
                    </svg>
                </a>
            </div>
        </div>
    </div>
</footer>