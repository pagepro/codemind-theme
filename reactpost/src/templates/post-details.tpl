<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">
        <title>{% block title %}{{ title }}{% endblock %}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        {% include "head/head-links.tpl" %}
        {% include "head/styles.tpl" %}
    </head>
    <body>
        {% import "partials/header-alt.tpl" as l_header_alt %}
        {{ l_header_alt.render() }}
        <main class="l-main">
            <div class="l-sub-hero js-sub-hero" style="background-image: url(../static/img/pic_hero.jpg);">
                <div class="l-sub-hero__wrapper">
                    <div class="l-sub-hero__content">
                        <div class="l-inner">
                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
                                    <div class="c-post c-post--alt">
                                        <div class="c-post__header">
                                            <h2 class="c-post__header__heading">
                                            <a href="post-details.html" class="c-post__header__heading__link theme-font-2 theme-size-9 theme-weight-2 theme-l-height-2 theme-color-2">We have two really good<br class="u-hide"> reasons to remove toxins from devices.</a>
                                            </h2>
                                            <div class="c-post__header__desc">
                                                <div class="c-post-info c-post-info--alt">
                                                    <span class="c-post-info__author theme-color-4">
                                                        <span class="c-post-info__author__pre theme-font-1 theme-size-2 theme-style-2 theme-weight-3 theme-l-height-3">by</span>
                                                        <a class="c-post-info__author__name" href="author.html"><span class="theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 t-upper t-space-wide">Chris lojniewski</span></a>
                                                    </span>
                                                    <a href="#" class="c-post-info__date">
                                                        <time class="c-post-info__date__info theme-color-4 t-space-wide t-upper theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3" datetime="2016-11-05 00:00">05 november 2016</time>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="c-socials-share js-socials">
                        <div class="c-socials-share__inner">
                            <p class="c-socials-share__title t-upper theme-color-2 theme-font-1 theme-size-1 theme-weight-2 theme-l-height-3">Share</p>
                            <div class="c-socials-share__content">
                                <ul class="c-socials-share-list">
                                    <li class="c-socials-share-list__item">
                                        <a href="#" class="c-socials-share-list__item__link c-twitter">
                                            <svg class="o-icon o-icon--twitter">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#twitter"></use>
                                            </svg>
                                        </a>
                                    </li>
                                    <li class="c-socials-share-list__item">
                                        <a href="#" class="c-socials-share-list__item__link c-facebook">
                                            <svg class="o-icon o-icon--facebook">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#facebook"></use>
                                            </svg>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="l-inner">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
                        <div class="c-post theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4">
                            <p>We started our Full Material Disclosure program to identify all the substances we use in all the parts we use. We’ve already looked at more than 10,000 individual components, and we get data on more parts every day. We assess the different chemicals in those components using 18 different criteria. This helps us understand their effect on our health and on the environment.</p>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                                Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur.
                            </p>
                            <pre class="c-code">
                                <code class="language-javascript">
/*
 * This is some sample code to illustrate how things look!
 */
import Musician from './liverpool';
class Paul extends Musician {
    constructor(bass) {
        super(bass);
    }
    get fullName() {
        return 'Paul McCartney';
    }
    perform() {
        this.play(this.instrument);
        this.sing();
    }
}
export default Paul;
                                </code>
                            </pre>
                            <p>
                                Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur.
                            </p>
                        </div>
                        <ul class="c-categories-list">
                            <li class="c-categories-list__item">
                                <a href="category.html" class="c-categories-list__item__link theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 theme-color-4 u-decor-bottom">Design</a>
                            </li>
                            <li class="c-categories-list__item">
                                <a href="category.html" class="c-categories-list__item__link theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 theme-color-4 u-decor-bottom">User Experience</a>
                            </li>
                        </ul>
                        <div class="d-decor-parent d-decor-parent--with-small-gutter">
                            <div class="d-decor"></div>
                        </div>
                        <div class="c-comments-wrapper" id="comments">
                            <div class="c-comments-box js-comments">
                                <div class="c-comments-box__header theme-bg-5 theme-color-6">
                                    <span class="c-icon-wrapper">
                                        <svg class="o-icon o-icon--chat">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#chat"></use>
                                        </svg>
                                    </span>
                                    <span class="c-label theme-weight-2">Comments</span>
                                </div>
                                <div class="c-comments-box__content">
                                    <ul class="c-comments-list">
                                        <li class="c-comments-list__item">
                                            <div class="c-comment">
                                                <div class="c-comment__avatar">
                                                    <figure class="o-img-wrapper">
                                                        <img src="./static/img/pic_avatar.jpg" alt="Avatar">
                                                    </figure>
                                                </div>
                                                <div class="c-comment__content">
                                                    <div class="c-comment__content__top">
                                                        <div class="c-comment__content__top__author">
                                                            <p class="theme-weight-2 theme-size-5">Ben</p>
                                                            <span class="theme-color-5 theme-weight-2 theme-size-4">Posted on
                                                            <time class="theme-color-4" datetime="2016-03-09 10:37">10:37 am 9th March 2014</time></span>
                                                        </div>
                                                        <div class="c-comment__content__top__action">
                                                            <a href="#" class="s-cta-2 theme-bg-5 theme-color-4">
                                                                <span class="theme-size-2 theme-weight-2">Reply</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="c-comment__content__desc theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vestibulum orci molestie dui aliquam, porta semper eros tempus. Sed convallis blandit eros et fringilla. Aenean id velit metus. Ut molestie luctus tincidunt. Integer hendrerit elit a diam eleifend, vel tempus tortor euismod. Quisque varius urna sit amet eleifend viverra.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <ul class="c-reply-list">
                                                <li class="c-reply-list__item">
                                                    <div class="c-comment">
                                                        <div class="c-comment__avatar">
                                                            <figure class="o-img-wrapper">
                                                                <img src="./static/img/pic_avatar.jpg" alt="Avatar">
                                                            </figure>
                                                        </div>
                                                        <div class="c-comment__content">
                                                            <div class="c-comment__content__top">
                                                                <div class="c-comment__content__top__author">
                                                                    <p class="theme-weight-2 theme-size-5">John</p>
                                                                    <span class="theme-color-5 theme-weight-2 theme-size-4">Posted on
                                                                    <time class="theme-color-4" datetime="2016-03-09 10:37">10:37 am 9th March 2014</time></span>
                                                                </div>
                                                                <div class="c-comment__content__top__action">
                                                                    <a href="#" class="s-cta-2 theme-bg-5 theme-color-4">
                                                                        <span class="theme-size-2 theme-weight-2">Reply</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="c-comment__content__desc theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4">
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vestibulum orci molestie dui aliquam, porta semper eros tempus. Sed convallis blandit eros et fringilla. Aenean id velit metus. Ut molestie luctus tincidunt. Integer hendrerit elit a diam eleifend, vel tempus tortor euismod. Quisque varius urna sit amet eleifend viverra.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="c-reply-list__item">
                                                    <div class="c-comment">
                                                        <div class="c-comment__avatar">
                                                            <figure class="o-img-wrapper">
                                                                <img src="./static/img/pic_avatar.jpg" alt="Avatar">
                                                            </figure>
                                                        </div>
                                                        <div class="c-comment__content">
                                                            <div class="c-comment__content__top">
                                                                <div class="c-comment__content__top__author">
                                                                    <p class="theme-weight-2 theme-size-5">Lucas</p>
                                                                    <span class="theme-color-5 theme-weight-2 theme-size-4">Posted on
                                                                    <time class="theme-color-4" datetime="2016-03-09 10:37">10:37 am 9th March 2014</time></span>
                                                                </div>
                                                                <div class="c-comment__content__top__action">
                                                                    <a href="#" class="s-cta-2 theme-bg-5 theme-color-4">
                                                                        <span class="theme-size-2 theme-weight-2">Reply</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="c-comment__content__desc theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4">
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vestibulum orci molestie dui aliquam, porta semper eros tempus. Sed convallis blandit eros et fringilla. Aenean id velit metus. Ut molestie luctus tincidunt. Integer hendrerit elit a diam eleifend, vel tempus tortor euismod. Quisque varius urna sit amet eleifend viverra.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="c-comments-list__item">
                                            <div class="c-comment">
                                                <div class="c-comment__avatar">
                                                    <figure class="o-img-wrapper">
                                                        <img src="./static/img/pic_avatar.jpg" alt="Avatar">
                                                    </figure>
                                                </div>
                                                <div class="c-comment__content">
                                                    <div class="c-comment__content__top">
                                                        <div class="c-comment__content__top__author">
                                                            <p class="theme-weight-2 theme-size-5">Harvey</p>
                                                            <span class="theme-color-5 theme-weight-2 theme-size-4">Posted on
                                                            <time class="theme-color-4" datetime="2016-03-09 10:37">10:37 am 9th March 2014</time></span>
                                                        </div>
                                                        <div class="c-comment__content__top__action">
                                                            <a href="#" class="s-cta-2 theme-bg-5 theme-color-4">
                                                                <span class="theme-size-2 theme-weight-2">Reply</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="c-comment__content__desc theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vestibulum orci molestie dui aliquam, porta semper eros tempus. Sed convallis blandit eros et fringilla. Aenean id velit metus. Ut molestie luctus tincidunt. Integer hendrerit elit a diam eleifend, vel tempus tortor euismod. Quisque varius urna sit amet eleifend viverra.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <form class="f-form f-form--comments">
                                        <div class="row" data-css-spacing="bottom1">
                                            <div class="col-xs-12">
                                                <div class="f-field f-field--textarea">
                                                    <div class="f-textarea-wrapper">
                                                        <textarea class="f-textarea" name="txt1" id="txt1" placeholder="Add your comment here..."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" data-css-spacing="bottom1">
                                            <div class="col-xs-12 col-sm-4" data-css-spacing="phone(bottom1)">
                                                <div class="f-field f-field--text">
                                                    <div class="f-input-wrapper">
                                                        <input type="text" class="f-input" placeholder="Name (Required)">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4" data-css-spacing="phone(bottom1)">
                                                <div class="f-field f-field--text">
                                                    <div class="f-input-wrapper">
                                                        <input type="email" class="f-input" placeholder="Email (Required)">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4">
                                                <div class="f-field f-field--text">
                                                    <div class="f-input-wrapper">
                                                        <input type="text" class="f-input" placeholder="Website URL">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <button type="submit" class="s-cta-1 theme-bg-3 theme-color-2">
                                                    <span class="c-label theme-weight-2 theme-size-3">Post Comment</span>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <a href="#comments" class="s-cta-1 js-comment-trigger theme-bg-3 theme-color-2 js-anim-rewind">
                                <span class="c-icon-wrapper">
                                    <svg class="o-icon o-icon--chat">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#chat"></use>
                                    </svg>
                                </span>
                                <span class="c-label theme-weight-2 theme-size-3">View Comments (<span>2</span>)...</span>
                            </a>
                        </div>
                        <div class="d-decor-parent d-decor-parent--with-small-gutter">
                            <div class="d-decor"></div>
                        </div>
                        <div class="c-related-post" data-css-spacing="top7 tablet(top5) phone(top3)">
                            <p class="c-related-post__title t-upper theme-font-1 theme-size-5 theme-weight-1 theme-l-height-3">Related articles:
                            </p>
                            <div class="c-related-post__content">
                                <div class="c-post">
                                    <div class="c-post__header">
                                        <h2 class="c-post__header__heading">
                                        <a href="post-details.html" class="c-post__header__heading__link theme-font-2 theme-size-9 theme-weight-2 theme-l-height-2 t-space-wide">We have two really good<br class="u-hide"> reasons to remove toxins from devices.</a>
                                        </h2>
                                        <div class="c-post__header__desc">
                                            <div class="c-post-info">
                                                <span class="c-post-info__author theme-color-4">
                                                    <span class="c-post-info__author__pre theme-font-1 theme-size-2 theme-style-2 theme-weight-3 theme-l-height-3">by</span>
                                                    <a class="c-post-info__author__name" href="author.html"><span class="theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 t-upper t-space-wide">Chris lojniewski</span></a>
                                                </span>
                                                <a href="#" class="c-post-info__date">
                                                    <time class="c-post-info__date__info theme-color-4 t-space-wide t-upper theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3" datetime="2016-11-05 00:00">05 november 2016</time>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-decor-parent d-decor-parent--with-small-gutter">
                                    <div class="d-decor"></div>
                                </div>
                                <div class="c-post">
                                    <div class="c-post__header">
                                        <h2 class="c-post__header__heading">
                                        <a href="post-details.html" class="c-post__header__heading__link theme-font-2 theme-size-9 theme-weight-2 theme-l-height-2">We have two really good<br class="u-hide"> reasons to remove toxins from devices.</a>
                                        </h2>
                                        <div class="c-post__header__desc">
                                            <div class="c-post-info">
                                                <span class="c-post-info__author theme-color-4">
                                                    <span class="c-post-info__author__pre theme-font-1 theme-size-2 theme-style-2 theme-weight-3 theme-l-height-3">by</span>
                                                    <a class="c-post-info__author__name" href="author.html"><span class="theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 t-upper t-space-wide">Chris lojniewski</span></a>
                                                </span>
                                                <a href="#" class="c-post-info__date">
                                                    <time class="c-post-info__date__info theme-color-4 t-space-wide t-upper theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3" datetime="2016-11-05 00:00">05 november 2016</time>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-decor-parent d-decor-parent--with-small-gutter">
                                    <div class="d-decor"></div>
                                </div>
                                <div class="c-post">
                                    <div class="c-post__header">
                                        <h2 class="c-post__header__heading">
                                        <a href="post-details.html" class="c-post__header__heading__link theme-font-2 theme-size-9 theme-weight-2 theme-l-height-2">We have two really good<br class="u-hide"> reasons to remove toxins from devices.</a>
                                        </h2>
                                        <div class="c-post__header__desc">
                                            <div class="c-post-info">
                                                <span class="c-post-info__author theme-color-4">
                                                    <span class="c-post-info__author__pre theme-font-1 theme-size-2 theme-style-2 theme-weight-3 theme-l-height-3">by</span>
                                                    <a class="c-post-info__author__name" href="author.html"><span class="theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 t-upper t-space-wide">Chris lojniewski</span></a>
                                                </span>
                                                <a href="#" class="c-post-info__date">
                                                    <time class="c-post-info__date__info theme-color-4 t-space-wide t-upper theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3" datetime="2016-11-05 00:00">05 november 2016</time>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-decor-parent d-decor-parent--with-small-gutter">
                                    <div class="d-decor"></div>
                                </div>
                                <div class="c-post">
                                    <div class="c-post__header">
                                        <h2 class="c-post__header__heading">
                                        <a href="post-details.html" class="c-post__header__heading__link theme-font-2 theme-size-9 theme-weight-2 theme-l-height-2">We have two really good<br class="u-hide"> reasons to remove toxins from devices.</a>
                                        </h2>
                                        <div class="c-post__header__desc">
                                            <div class="c-post-info">
                                                <span class="c-post-info__author theme-color-4">
                                                    <span class="c-post-info__author__pre theme-font-1 theme-size-2 theme-style-2 theme-weight-3 theme-l-height-3">by</span>
                                                    <a class="c-post-info__author__name" href="author.html"><span class="theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 t-upper t-space-wide">Chris lojniewski</span></a>
                                                </span>
                                                <a href="#" class="c-post-info__date">
                                                    <time class="c-post-info__date__info theme-color-4 t-space-wide t-upper theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3" datetime="2016-11-05 00:00">05 november 2016</time>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-decor-parent d-decor-parent--with-big-gutter">
                            <div class="d-decor"></div>
                        </div>
                        <div class="c-post-navigation">
                            <a href="#" class="c-post-navigation__prev is-disabled">
                                <svg class="o-icon o-icon--arrow-left">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#arrow-left"></use>
                                </svg>
                            </a>
                            <a href="#" class="c-post-navigation__next">
                                <svg class="o-icon o-icon--arrow-right">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#arrow-right"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        {% include "partials/footer.tpl" %}
        <script src="./static/js/app.js"></script>
    </body>
</html>