<?php
get_header();
?>
<main class="l-main">
	<div class="l-inner">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
				<div class="c-msg c-msg--default theme-bg-3">
					<h1 class="c-msg__inner">
						<span class="c-label theme-color-2 theme-font-1 theme-size-4 theme-weight-2 theme-l-height-2 opacity-1"><?php esc_html_e('Author', 'code-mind'); ?></span>
						<span class="c-label theme-color-2 theme-font-1 theme-size-4 theme-weight-2 theme-l-height-2"><?php echo get_queried_object()->display_name; ?></span>
						<a href="<?php echo esc_url(home_url('/')); ?>" class="c-msg__action theme-color-2 theme-weight-2" title="Close">
							<svg class="o-icon o-icon--close">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#close"></use>
							</svg>
						</a>
					</h1>
				</div>
				<?php
					global $wp_query;
					$user_query = $wp_query->get_queried_object();
					$author_post_meta = get_user_meta($user_query->ID);
				?>
				<div class="c-author">
					<div class="c-author__avatar">
						<figure class="o-img-wrapper">
							<img src="<?php echo get_avatar_url($user_query->ID); ?>" alt="<?php echo $user_query->display_name; ?>" />
						</figure>
					</div>
					<div class="c-author__content theme-font-1 theme-size-5 theme-weight-1 theme-l-height-4">
						<div class="c-author__content__top">
							<a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>">
								<?php echo $user_query->display_name; ?>
							</a>
							<?php if($user_query->user_url) : ?>
								<p>
									<a href="<?php echo $user_query->user_url; ?>" class="theme-weight-2 theme-font-1 theme-size-4 theme-l-height-3 theme-color-3 u-default-link-anim">
										<?php echo $user_query->user_url; ?>
									</a>
								</p>
							<?php endif; ?>
						</div>
						<?php if($author_post_meta['description'][0]): ?>
							<div class="c-author__content__desc theme-font-1 theme-size-3 theme-weight-1 theme-l-height-1 theme-color-4">
								<?php echo $author_post_meta['description'][0]; ?>
							</div>
						<?php endif; ?>
						<div class="c-author__content__socials">
							<ul class="c-socials-share-list c-socials-share-list--alt">
								<?php if($author_post_meta['twitter'][0]): ?>
									<li class="c-socials-share-list__item">
										<a href="<?php echo $author_post_meta['twitter'][0]; ?>" target="_blank" class="c-socials-share-list__item__link c-twitter" rel="nofollow">
											<svg class="o-icon o-icon--twitter">
												<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#twitter"></use>
											</svg>
										</a>
									</li>
								<?php endif; ?>
								<?php if($author_post_meta['facebook'][0]): ?>
									<li class="c-socials-share-list__item">
										<a href="<?php echo $author_post_meta['facebook'][0]; ?>" target="_blank" class="c-socials-share-list__item__link c-facebook" rel="nofollow">
											<svg class="o-icon o-icon--facebook">
												<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#facebook"></use>
											</svg>
										</a>
									</li>
								<?php endif; ?>
								<?php if($author_post_meta['instagram'][0]): ?>
								<li class="c-socials-share-list__item">
									<a href="<?php echo $author_post_meta['instagram'][0]; ?>" target="_blank" class="c-socials-share-list__item__link c-instagram" rel="nofollow">
										<svg class="o-icon o-icon--instagram">
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#instagram"></use>
										</svg>
									</a>
								</li>
								<?php endif; ?>
								<?php if($author_post_meta['pinterest'][0]): ?>
									<li class="c-socials-share-list__item">
										<a href="<?php echo $author_post_meta['pinterest'][0]; ?>" target="_blank" class="c-socials-share-list__item__link c-pinterest" rel="nofollow">
											<svg class="o-icon o-icon--pinterest">
												<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#pinterest"></use>
											</svg>
										</a>
									</li>
								<?php endif; ?>
								<?php if($author_post_meta['snapchat'][0]): ?>
									<li class="c-socials-share-list__item">
										<a href="<?php echo $author_post_meta['snapchat'][0]; ?>" target="_blank" class="c-socials-share-list__item__link c-snapchat" rel="nofollow">
											<svg class="o-icon o-icon--snapchat">
												<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#snapchat"></use>
											</svg>
										</a>
									</li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>
				<div class="d-decor-parent d-decor-parent--with-big-gutter">
					<div class="d-decor"></div>
				</div>
				<?php
				if (have_posts()) :
					while (have_posts()) :
						the_post();
						$categories = get_the_category();
						?>
						<div class="c-post">
							<div class="c-post__categories">
								<ul class="c-categories-list">
									<?php if($categories): ?>
										<?php foreach ($categories as $category) : ?>
											<li class="c-categories-list__item">
												<a href="<?php echo get_category_link($category->term_id); ?>" class="c-categories-list__item__link theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 theme-color-4 u-decor-bottom"><?php echo $category->name; ?></a>
											</li>
										<?php endforeach; ?>
									<?php endif; ?>
								</ul>
							</div>
							<div class="c-post__header">
								<h2 class="c-post__header__heading">
									<a href="<?php the_permalink(); ?>" class="c-post__header__heading__link theme-font-2 theme-size-9 theme-weight-2 theme-l-height-2">
										<?php the_title(); ?>
									</a>
								</h2>
								<div class="c-post__header__desc">
									<div class="c-post-info">
										<span class="c-post-info__author theme-color-4">
											<span class="c-post-info__author__pre theme-font-1 theme-size-2 theme-style-2 theme-weight-3 theme-l-height-3">by</span>
											<?php the_author_posts_link(); ?>
										</span>
										<time class="c-post-info__date theme-color-4 t-space-wide t-upper theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3" datetime="<?php echo get_the_date('Y-m-d H:i'); ?>"><?php echo get_the_date('j F Y'); ?></time>
									</div>
								</div>
							</div>
							<div class="c-post__content">
								<div class="c-post__content__title">
									<p class="theme-font-1 theme-size-5 theme-l-height-3 t-weight-1"><?php echo get_post_meta(get_the_ID(), 'Second Excerpt', true); ?></p>
								</div>
								<div class="c-post__content__article">
									<div class="theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4">
										<?php the_excerpt(); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="d-decor-parent d-decor-parent--with-big-gutter">
							<div class="d-decor"></div>
						</div>
				<?php
					endwhile;
					get_template_part('template-parts/pagination');
				endif;
				?>
			</div>
		</div>
	</div>
</main>
<?php
get_footer();
