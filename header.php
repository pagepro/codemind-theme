<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package code-mind
 */

$custom_logo_id = get_theme_mod( 'custom_logo' );
$custom_alt_logo_id = get_theme_mod( 'alt_logo' );

$header_bg = get_template_directory_uri() . '/static/img/logo.png';
if (!is_singular()) {
    $header_bg = ($custom_logo_id) ? wp_get_attachment_image_src( $custom_logo_id , 'full' )[0] :  get_template_directory_uri() . '/static/img/logo.png';
} else {
    if(has_post_thumbnail()) {
        if(is_singular( 'insight' )) {
            $header_bg = ($custom_logo_id) ? wp_get_attachment_image_src( $custom_logo_id , 'full' )[0] :  get_template_directory_uri() . '/static/img/logo.png';
        } else {
            $header_bg = ($custom_alt_logo_id) ? $custom_alt_logo_id : get_template_directory_uri() . '/static/img/logo-alt.png';
        }
    } else {
        $header_bg = ($custom_logo_id) ? wp_get_attachment_image_src( $custom_logo_id , 'full' )[0] :  get_template_directory_uri() . '/static/img/logo.png';
    }
}

$header_bg_class = 'theme-color-1';
if (!is_singular()) {
    $header_bg_class = 'theme-color-1';
} else {
    if(!has_post_thumbnail()) {
        $header_bg_class = 'theme-color-1';
    } else {
        if(is_singular( 'insight' ) && has_post_thumbnail()) {
            $header_bg_class = 'theme-color-1';
        } else{
            $header_bg_class = 'theme-color-2';    
        }
    }
}

?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js" >
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <?php wp_head(); ?>
    </head>
    <body>
        <?php wp_body_open(); ?>
        <div class="l-wrapper">
            <header class="l-header js-sticky-header-alt">
                <div class="l-header__inner">
                    <div class="l-header__box js-header-menu-box">
                        <div class="l-header__menu-toggler js-header-menu-toggler">
                            <a href="#" class="c-menu-toggler js-menu-toggler">
                                <span class="c-menu-toggler__wrap">
                                    <span class="c-menu-toggler__lines"></span>
                                    <span class="c-menu-toggler__info"><?php _e('Menu','code-mind'); ?></span>
                                </span>
                            </a>
                        </div>
                        <div class="l-header__title js-header-title">
                            <div class="c-logo c-logo--main">
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="c-logo__inner">
                                    <img src="<?php echo $header_bg; ?>" alt="<?php echo get_bloginfo('name') ?>">
                                    <span class="c-label">
                                        <?php echo get_bloginfo('name'); ?>
                                    </span>
                                </a>
                            </div>
                            <?php if ( is_front_page() ) : ?>
                                <h1 class="c-subtitle theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4 t-upper <?php echo $header_bg_class; ?>"><?php bloginfo('description'); ?></h1>
                            <?php else : ?>
                                <p class="c-subtitle theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4 t-upper <?php echo $header_bg_class; ?>"><?php bloginfo('description'); ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="l-header__tools js-header-tools">
                            <div class="c-search c-search--alt">
                                <a href="#" class="c-search__toggler c-btn c-btn--icon js-search-toggler">
                                    <span class="c-btn__content">
                                        <svg class="o-icon o-icon--search">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#search"></use>
                                        </svg>
                                        <span class="c-label"><?php _e('Search','code-mind'); ?></span>
                                    </span>
                                </a>
                                <div class="c-search__wrapper theme-bg-1 js-search">
                                    <a href="#" class="c-search__close js-close-search" title="Close search">
                                        <svg class="o-icon o-icon--close">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#close"></use>
                                        </svg>
                                    </a>
                                    <form class="f-search-box t-center js-search-form" action="<?php bloginfo('url'); ?>">
                                        <div class="f-search-box__inner">
                                            <input name="s" type="text" class="f-search-box__input theme-font-1 theme-size-10 theme-l-height-3 t-weight-1 theme-color-2 js-search-input" placeholder="Search">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="l-header__box">
                        <div class="l-header__nav-container js-menu-nav">
                            <div class="l-header__nav">
                                <nav class="c-main-nav" aria-label="main navigation">
                                <div class="c-main-nav__toggler">
                                    <a href="#" class="c-menu-close js-menu-close" title="Close">
                                        <span class="c-menu-close__inner">
                                            <svg class="o-icon o-icon--close">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#close"></use>
                                            </svg>
                                        </span>
                                    </a>
                                </div>
                                <div class="c-main-nav__navigation">
                                    <?php
                                    if( has_nav_menu( 'menu-1' ) ):
                                        wp_nav_menu( array(
                                            'theme_location' => 'menu-1',
                                            'menu_class'     => 'c-main-nav-list',
                                            'container'      => 'false',
                                            'walker'         => new Code_Mind_Walker_Nav_Menu()
                                        ) );
                                    endif;
                                    if( has_nav_menu( 'menu-2' ) ):
                                    ?>
                                    <div class="c-categories-nav">
                                        <p class="c-categories-nav__title theme-color-2 t-upper theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4"><?php _e('Categories:','code-mind'); ?></p>
                                        <?php
                                            wp_nav_menu( array(
                                                'theme_location' => 'menu-2',
                                                'menu_class'     => 'c-categories-nav-list',
                                                'container'      => 'false',
                                                'walker'         => new Code_Mind_Walker_Nav_Menu()
                                            ) );
                                        ?>
                                    </div>
                                    <?php endif; ?>
                                    <?php dynamic_sidebar( 'widgets' ); ?>
                                </div>
                                <div class="c-main-nav__socials">
                                    <p class="c-simple-text theme-color-6 theme-font-1 theme-size-4 theme-style-2 theme-weight-2 theme-l-height-2">
                                        <?php _e('Stay in touch','code-mind'); ?>
                                    </p>
                                    <?php
                                        dynamic_sidebar( 'social-media' );
                                    ?>
                                </div>
                            </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </header>