<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package code-mind
 */

get_header();
?>
<main class="l-main">
	<div class="l-inner">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
				<?php
				if (have_posts()) :
					while (have_posts()) :
						the_post();
						$categories = get_the_category();
						$template = get_page_template_slug( get_the_ID() );
				?>
					<?php if ( $template !== 'template-reactpost.php' ) : ?>
						<div class="c-post">
							<div class="c-post__categories">
								<ul class="c-categories-list">
									<?php foreach ($categories as $category) : ?>
										<li class="c-categories-list__item">
											<a href="<?php echo get_category_link($category->term_id); ?>" class="c-categories-list__item__link theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 theme-color-4 u-decor-bottom"><?php echo $category->name; ?></a>
										</li>
									<?php endforeach; ?>
								</ul>
							</div>
							<div class="c-post__header">
								<h2 class="c-post__header__heading">
									<a href="<?php the_permalink(); ?>" class="c-post__header__heading__link theme-font-2 theme-size-9 theme-weight-2 theme-l-height-2">
										<?php the_title(); ?>
									</a>
								</h2>
								<div class="c-post__header__desc">
									<div class="c-post-info">
										<span class="c-post-info__author theme-color-4">
											<span class="c-post-info__author__pre theme-font-1 theme-size-2 theme-style-2 theme-weight-3 theme-l-height-3">by</span>
											<?php the_author_posts_link(); ?>
										</span>
										<time class="c-post-info__date theme-color-4 t-space-wide t-upper theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3" datetime="<?php echo get_the_date('Y-m-d H:i'); ?>"><?php echo get_the_date('j F Y'); ?></time>
									</div>
								</div>
							</div>
							<div class="c-post__content">
								<div class="c-post__content__title">
									<p class="theme-font-1 theme-size-5 theme-l-height-3 t-weight-1"><?php echo get_post_meta(get_the_ID(), 'Second Excerpt', true); ?></p>
								</div>
								<div class="c-post__content__article">
									<div class="theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4">
										<?php the_excerpt(); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="d-decor-parent d-decor-parent--with-big-gutter">
							<div class="d-decor"></div>
						</div>
					<?php endif; ?>
				<?php
					endwhile;
					get_template_part('template-parts/pagination');
				else :
					esc_html_e('Sorry, no posts matched your criteria.');

				endif;
				?>
			</div>
		</div>
	</div>
</main>
<?php
get_footer();
