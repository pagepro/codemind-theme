<?php
/*
 * Template Name: React post
 * Template Post Type: post
 */
get_header( 'reactpost' );
$path = get_template_directory_uri();
$post_link = get_the_permalink();
?>
<div class="l-container l-container--custom-blog-post js-sticky-navigation-trigger">
    <div class="c-sticky-navigation-wrapper js-sticky-navigation-box">
        <div class="c-sticky-navigation ui-animation-hook ui-animation-hook--left">
            <div class="c-sticky-navigation__inner">
                <ul class="c-sticky-navigation__list js-sticky-navigation-list">
                </ul>
            </div>
        </div>
    </div>
    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>
    <?php endif; ?>
    <div class="c-share-social-wrapper">
        <div class="c-share-social l-inner">
            <div class="c-share-social__inner">
                <h3 class="c-share-social__heading">
                    <?php esc_html_e('Don’t forget to share this post!', 'codemind'); ?>
                </h3>
                <ul class="c-share-social__list">
                    <li class="c-share-social__item">
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $post_link; ?>" rel="nofollow noopener noreferrer" target="_blank" class="c-share-social__item__link">
                            <svg class="o-icon o-icon--facebook">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $path; ?>/svg/static_symbol_svg_sprite.symbol.svg#facebook"></use>
                            </svg>
                        </a>
                    </li>
                    <li class="c-share-social__item">
                        <a href="https://twitter.com/share?text=<?php the_title(); ?>&amp;url=<?php echo $post_link; ?>" rel="nofollow noopener noreferrer" target="_blank" class="c-share-social__item__link">
                            <svg class="o-icon o-icon--twitter">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $path; ?>/svg/static_symbol_svg_sprite.symbol.svg#twitter"></use>
                            </svg>
                        </a>
                    </li>
                    <li class="c-share-social__item">
                        <a href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo $post_link; ?>" rel="nofollow noopener noreferrer" target="_blank" class="c-share-social__item__link">
                            <svg class="o-icon o-icon--linkedin">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $path; ?>/svg/static_symbol_svg_sprite.symbol.svg#linkedin"></use>
                            </svg>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php
get_footer( 'reactpost' );
?>
