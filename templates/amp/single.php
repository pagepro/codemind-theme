<?php
/**
 * Single view template.
 *
 * @package AMP
 */

/**
 * Context.
 *
 * @var AMP_Post_Template $this
 */

$this->load_parts( [ 'html-start' ] );
?>

<?php $this->load_parts( [ 'header' ] ); ?>

<article class="amp-wp-article">
	<header class="amp-wp-article-header">
    <?php
    $categories = get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'amp' ), '', $this->ID );
    if ( $categories ) : ?>
        <div class="amp-wp-meta amp-wp-tax-category">
            <?php
            /* translators: %s: list of categories. */
            printf( $categories ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
            ?>
        </div>
    <?php endif; ?>
		<h1 class="amp-wp-title"><?php echo $this->get( 'post_title' ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></h1>
		<?php
		/**
		 * Filters the template parts loaded in the header area of the AMP legacy post template.
		 *
		 * @since 0.4
		 * @param string[] Templates to load.
		 */
        $post_author = $this->get( 'post_author' );
        if ( $post_author ) : ?>
            <div class="amp-wp-meta amp-wp-byline">
                <?php if ( function_exists( 'get_avatar_url' ) ) : ?>
                    <amp-img src="<?php echo esc_url( get_avatar_url( $post_author->user_email, [ 'size' => 24 ] ) ); ?>" alt="<?php echo esc_attr( $post_author->display_name ); ?>" width="24" height="24" layout="fixed"></amp-img>
                <?php endif; ?>
                <span class="amp-wp-author author vcard"><?php echo esc_html( sprintf( __( 'by %s', 'amp' ),  $post_author->display_name ) ); ?></span>
            </div>
        <?php endif; ?>
        <div class="amp-wp-meta amp-wp-posted-on">
            <time datetime="<?php echo esc_attr( gmdate( 'c', $this->get( 'post_publish_timestamp' ) ) ); ?>">
                <?php
                echo esc_html(
                    sprintf(
                        /* translators: %s: the human-readable time difference. */
                        __( '%s ago', 'amp' ),
                        human_time_diff( $this->get( 'post_publish_timestamp' ), time() )
                    )
                );
                ?>
            </time>
        </div>
	</header>

	<?php $this->load_parts( [ 'featured-image' ] ); ?>

	<div class="amp-wp-article-content">
		<?php echo $this->get( 'post_amp_content' ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
	</div>

	<footer class="amp-wp-article-footer">
        <?php
            $tags = get_the_tag_list(
                '',
                _x( ', ', 'Used between list items, there is a space after the comma.', 'amp' ),
                '',
                $this->ID
            );
        ?>
        <?php if ( $tags && ! is_wp_error( $tags ) ) : ?>
            <div class="amp-wp-meta amp-wp-tax-tag">
                <?php
                    /* translators: %s: list of tags. */
                    echo ( $tags ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                ?>
            </div>
        <?php endif; ?>
		<?php
		/**
		 * Filters the template parts to load in the footer area of the AMP legacy post template.
		 *
		 * @since 0.4
		 * @param string[] Templates to load.
		 */
		$this->load_parts( apply_filters( 'amp_post_article_footer_meta', [ 'meta-comments-link' ] ) );
		?>
	</footer>
</article>

<?php $this->load_parts( [ 'footer' ] ); ?>

<?php
$this->load_parts( [ 'html-end' ] );
