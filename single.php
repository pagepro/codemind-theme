<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package code-mind
 */

get_header();
$query_facebook = 'https://www.facebook.com/sharer.php';
$new_query_facebook = add_query_arg(array(
	'u'     => get_the_permalink(),
	't' => get_the_title()
), $query_facebook);

$query_twitter = 'https://twitter.com/intent/tweet';
$new_query_twitter = add_query_arg(array(
	'text'  => get_the_title(),
	'url'     => get_the_permalink()
), $query_twitter);

$categories = array();
foreach(get_the_category($post->ID) as $category) {
	array_push($categories, $category->cat_ID);
}

$news = array(
	'post_type' => 'post',
	'category__in' => $categories,
	'post__not_in' => array($post->ID),
	'posts_per_page' => 2
);
$all_news = new WP_Query($news);
?>

<main class="l-main">
	<?php
	while (have_posts()) :
		the_post();
		$featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');
		$categories = get_the_category();
		$author_post_meta = get_user_meta(get_the_author_ID());
		?>
		<div class="l-sub-hero <?php echo ($featured_img_url) ? 'l-sub-hero--background' : '' ?> js-sub-hero" style="<?php echo ($featured_img_url) ? 'background-image: url(' . $featured_img_url . ');' : '' ?>">
			<div class="l-sub-hero__wrapper">
				<div class="l-sub-hero__content<?php echo ($featured_img_url) ? '--background' : '' ?>">
					<div class="l-inner">
						<div class="row">
							<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
								<div class="c-post c-post--alt">
									<div class="c-post__header">
										<div class="c-post__header__desc">
											<div class="c-post-info c-post-info<?php echo ($featured_img_url) ? '--alt' : '' ?>">
												<span class="c-post-info__author theme-color-4">
													<ul class="c-categories-list">
														<?php if($categories) : ?>
															<?php foreach ($categories as $category) : ?>
																<li class="c-categories-list__item c-post-info__author theme-color-4">
																	<a href="<?php echo get_category_link($category->term_id); ?>" class="c-post-info__author__name theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 t-upper u-decor-bottom"><?php echo $category->name; ?></a>
																</li>
															<?php endforeach; ?>
														<?php endif; ?>
													</ul>
												</span>
											</div>
										</div>
										<h1 class="c-post__header__heading theme-font-2 theme-size-9 theme-weight-2 theme-l-height-2 theme-color-<?php echo ($featured_img_url) ? '2' : '1'; ?>">
											<?php the_title(); ?>
										</h1>
										<div class="c-post__header__desc">
											<div class="c-post-info c-post-info<?php echo ($featured_img_url) ? '--alt' : '' ?>">
												<span class="c-post-info__author theme-color-4">
													<span class="c-post-info__author__pre theme-font-1 theme-size-2 theme-style-2 theme-weight-3 theme-l-height-3">by</span>
													<?php the_author_posts_link(); ?>
												</span>
												<time class="c-post-info__date theme-color-4 t-space-wide t-upper theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3" datetime="<?php echo get_the_date('Y-m-d H:i'); ?>"><?php echo get_the_date('j F Y'); ?></time>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="c-socials-share js-socials">
					<div class="c-socials-share__inner">
						<p class="c-socials-share__title t-upper theme-color-2 theme-font-1 theme-size-1 theme-weight-2 theme-l-height-3"><?php esc_html_e('Share', 'code-mind'); ?></p>
						<div class="c-socials-share__content">
							<ul class="c-socials-share-list">
								<li class="c-socials-share-list__item">
									<a href="<?php echo $new_query_twitter; ?>" class="c-socials-share-list__item__link c-twitter" target="_blank">
										<svg class="o-icon o-icon--twitter">
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#twitter"></use>
										</svg>
									</a>
								</li>
								<li class="c-socials-share-list__item">
									<a href="<?php echo $new_query_facebook; ?>" class="c-socials-share-list__item__link c-facebook" target="_blank">
										<svg class="o-icon o-icon--facebook">
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#facebook"></use>
										</svg>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="l-inner">
			<div class="row">
				<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
					<div class="c-post theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4">
						<?php the_content(); ?>
					</div>
					<?php if(get_the_tags()): ?>
						<ul class="c-categories-list">
							<?php foreach(get_the_tags() as $post_tag) : ?>
								<li class="c-categories-list__item">
									<a href="<?php echo get_tag_link($post_tag->term_id); ?>" class="c-categories-list__item__link theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 theme-color-4 u-decor-bottom">
										<?php echo $post_tag->name; ?>
									</a>
								</li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
					<div class="d-decor-parent d-decor-parent--with-small-gutter">
						<div class="d-decor"></div>
					</div>
					<div class="c-post__footer theme-font-1 theme-size-5 theme-weight-1 theme-l-height-4">
						<div class="c-post__profile-avatar">
							<img src="<?php echo get_avatar_url(get_the_author_meta( 'ID' )); ?>" alt="<?php echo get_the_author_meta( 'display_name' ) ?>" />
						</div>
						<div class="c-post__profile-content">
							<a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>">
								<?php the_author(); ?>
							</a>
							<?php if(get_the_author_url()) : ?>
								<p>
									<a href="<?php echo get_the_author_url(); ?>" class="theme-weight-2 theme-font-1 theme-size-4 theme-l-height-3 theme-color-3 u-default-link-anim">
										<?php echo get_the_author_url(); ?>
									</a>
								</p>
							<?php endif; ?>
							<p class="theme-font-1 theme-size-3 theme-weight-1 theme-l-height-1 theme-color-4"><?php echo get_the_author_meta('description'); ?></p>
							<div class="c-author__content__socials">
								<ul class="c-socials-share-list c-socials-share-list--alt">
									<?php if($author_post_meta['twitter'][0]): ?>
										<li class="c-socials-share-list__item">
											<a href="<?php echo $author_post_meta['twitter'][0]; ?>" target="_blank" class="c-socials-share-list__item__link c-twitter">
												<svg class="o-icon o-icon--twitter">
													<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#twitter"></use>
												</svg>
											</a>
										</li>
									<?php endif; ?>
									<?php if($author_post_meta['facebook'][0]): ?>
										<li class="c-socials-share-list__item">
											<a href="<?php echo $author_post_meta['facebook'][0]; ?>" target="_blank" class="c-socials-share-list__item__link c-facebook">
												<svg class="o-icon o-icon--facebook">
													<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#facebook"></use>
												</svg>
											</a>
										</li>
									<?php endif; ?>
									<?php if($author_post_meta['instagram'][0]): ?>
									<li class="c-socials-share-list__item">
										<a href="<?php echo $author_post_meta['instagram'][0]; ?>" target="_blank" class="c-socials-share-list__item__link c-instagram">
											<svg class="o-icon o-icon--instagram">
												<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#instagram"></use>
											</svg>
										</a>
									</li>
									<?php endif; ?>
									<?php if($author_post_meta['pinterest'][0]): ?>
										<li class="c-socials-share-list__item">
											<a href="<?php echo $author_post_meta['pinterest'][0]; ?>" target="_blank" class="c-socials-share-list__item__link c-pinterest">
												<svg class="o-icon o-icon--pinterest">
													<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#pinterest"></use>
												</svg>
											</a>
										</li>
									<?php endif; ?>
									<?php if($author_post_meta['snapchat'][0]): ?>
										<li class="c-socials-share-list__item">
											<a href="<?php echo $author_post_meta['snapchat'][0]; ?>" target="_blank" class="c-socials-share-list__item__link c-snapchat">
												<svg class="o-icon o-icon--snapchat">
													<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#snapchat"></use>
												</svg>
											</a>
										</li>
									<?php endif; ?>
								</ul>
							</div>
						</div>
					</div>
					<div class="d-decor-parent d-decor-parent--with-small-gutter">
						<div class="d-decor"></div>
					</div>
					<!-- Comments -->
					<div class="c-comments-wrapper" id="comments">
						<div class="c-comments-box js-comments">
							<div class="c-comments-box__header theme-bg-5 theme-color-6">
								<span class="c-icon-wrapper">
									<svg class="o-icon o-icon--chat">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#chat"></use>
									</svg>
								</span>
								<span class="c-label theme-weight-2"><?php _e('Comments', 'codemind'); ?></span>
							</div>
							<?php
							$args = array(
								'post_id' => $post->ID,
								'status' => 'approve',
							);
							$comments = get_comments($args);
							?>
							<div class="c-comments-box__content">
								<ul class="c-comments-list js-comments-list">
									<?php foreach ($comments as $comment) : ?>
										<?php if ($comment->comment_parent == 0) : ?>
											<li class="c-comments-list__item js-comments-item">
												<div class="c-comment">
													<div class="c-comment__avatar">
														<figure class="o-img-wrapper">
															<img src="<?php echo get_avatar_url($comment->comment_author_email) ?>" alt="<?php echo $comment->comment_author; ?>" />
														</figure>
													</div>
													<div class="c-comment__content">
														<div class="c-comment__content__top">
															<div class="c-comment__content__top__author">
																<p class="theme-weight-2 theme-size-5"><?php echo $comment->comment_author; ?></p>
																<span class="theme-color-5 theme-weight-2 theme-size-4">
																	Posted on
																	<time class="theme-color-4" datetime="<?php echo get_the_date('Y-m-d H:i'); ?>"><?php echo $comment->comment_date; ?></time>
																</span>
															</div>
															<div class="c-comment__content__top__action">
																<?php
																$max_depth = get_option('thread_comments_depth');
																$default = array(
																	'add_below'  => 'comment',
																	'respond_id' => 'respond',
																	'reply_text' => '<span class="s-cta-2 theme-bg-5 theme-color-4 theme-size-2 theme-weight-2 js-comment-reply" data-comment-reply-text="'.__('Leave a Reply to comment', 'codemind').'" data-comment-reply-cancel="'.__('Cancel reply', 'codemind').'">' . __('Reply', 'codemind') . '</span>',
																	'login_text' => __('Log in to Reply'),
																	'depth'      => 1,
																	'before'     => '',
																	'after'      => '',
																	'max_depth'  => $max_depth
																);
																comment_reply_link($default, $comment->comment_ID, $post->ID);
																?>
															</div>
														</div>
														<div class="c-comment__content__desc theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4">
															<p><?php echo $comment->comment_content; ?></p>
														</div>
													</div>
												</div>
												<?php
												$childcomments = get_comments(array(
													'post_id'   => $post->ID,
													'status'    => 'approve',
													'parent'    => $comment->comment_ID,
												));
												?>
												<?php if ($childcomments) : ?>
													<ul class="c-reply-list">
														<?php foreach ($childcomments as $childcomment) : ?>
															<li class="c-reply-list__item">
																<div class="c-comment">
																	<div class="c-comment__avatar">
																		<figure class="o-img-wrapper">
																			<img src="<?php echo get_avatar_url($childcomment->comment_author_email) ?>" alt="<?php echo $childcomment->comment_author; ?>" />
																		</figure>
																	</div>
																	<div class="c-comment__content">
																		<div class="c-comment__content__top">
																			<div class="c-comment__content__top__author">
																				<p class="theme-weight-2 theme-size-5"><?php echo $childcomment->comment_author; ?></p>
																				<span class="theme-color-5 theme-weight-2 theme-size-4">
																					Posted on
																					<time class="theme-color-4" datetime="<?php echo get_the_date('Y-m-d H:i'); ?>"><?php echo $childcomment->comment_date; ?></time>
																				</span>
																			</div>
																		</div>
																		<div class="c-comment__content__desc theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4">
																			<p><?php echo $childcomment->comment_content; ?></p>
																		</div>
																	</div>
																</div>
															</li>
														<?php endforeach; ?>
													</ul>
												<?php endif; ?>
											</li>
										<?php endif; ?>
									<?php endforeach; ?>
								</ul>
							</div>
							<?php get_template_part('template-parts/comment-form'); ?>
						</div>
						<a href="#comments" class="s-cta-1 js-comment-trigger theme-bg-3 theme-color-2 js-anim-rewind">
							<span class="c-icon-wrapper">
								<svg class="o-icon o-icon--chat">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#chat"></use>
								</svg>
							</span>
							<span class="c-label theme-weight-2 theme-size-3">View Comments (<span><?php echo get_comments_number($post->ID); ?></span>)...</span>
						</a>
					</div>
					<div class="d-decor-parent d-decor-parent--with-small-gutter">
						<div class="d-decor"></div>
					</div>
					<!-- Related post -->
					<?php if ($all_news->have_posts()) : ?>
						<div class="c-related-post" data-css-spacing="top7 tablet(top5) phone(top3)">
							<p class="c-related-post__title t-upper theme-font-1 theme-size-5 theme-weight-1 theme-l-height-3">
								<?php esc_html_e('Related articles:', 'code-mind'); ?>
							</p>
							<div class="c-related-post__content">
								<?php while ($all_news->have_posts()): ?>
									<?php $all_news->the_post(); ?>
									<div class="c-post">
										<div class="c-post__header">
											<h2 class="c-post__header__heading">
												<a href="<?php the_permalink(); ?>" class="c-post__header__heading__link theme-font-2 theme-size-9 theme-weight-2 theme-l-height-2 t-space-wide">
													<?php the_title(); ?>
												</a>
											</h2>
											<div class="c-post__header__desc">
												<div class="c-post-info">
													<span class="c-post-info__author theme-color-4">
														<span class="c-post-info__author__pre theme-font-1 theme-size-2 theme-style-2 theme-weight-3 theme-l-height-3">by</span>
														<?php the_author_posts_link(); ?>
													</span>
													<time class="c-post-info__date theme-color-4 t-space-wide t-upper theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3" datetime="<?php echo get_the_date('Y-m-d H:i'); ?>"><?php the_date('j F Y'); ?></time>
												</div>
											</div>
										</div>
									</div>
									<div class="d-decor-parent d-decor-parent--with-small-gutter">
										<div class="d-decor"></div>
									</div>
								<?php endwhile; ?>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php
		if (comments_open() || get_comments_number()) :
		// comments_template();
		endif;
	endwhile; // End of the loop.
	?>
</main>

<?php
get_footer();
