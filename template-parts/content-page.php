<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package code-mind
 */

?>

<div class="c-post <?php post_class(); ?>" id="post-<?php the_ID(); ?>">
	<div class="c-post__header">
		<h2 class="c-post__header__heading theme-font-2 theme-size-9 theme-weight-2 theme-l-height-2">
			<?php the_title(); ?>
		</h2>
	</div>
</div>
<div class="c-post__content">
	<div class="c-post__content__title">
		<p class="theme-font-1 theme-size-5 theme-l-height-3 t-weight-1"></p>
	</div>
	<div class="c-post__content__article">
		<div class="theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4">
			<?php the_content(); ?>
		</div>
	</div>
</div>
