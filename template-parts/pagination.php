<?php
$queried_cat = get_query_var( 'category_name' );
$queried_author = get_query_var( 'author_name' );
$tag = get_query_var( 'tag' );

$site_url = get_home_url();

$year     = get_query_var( 'year' );
$monthnum = get_query_var( 'monthnum' );
$day      = get_query_var( 'day' );

$search = ( isset( $_GET['s'] ) ) ? $_GET['s'] : '';

$queried_page = get_query_var( 'paged' ) === 0 ? 1 : get_query_var( 'paged' );
$queried_per_page = get_query_var( 'posts_per_page' );
$url_string = '';

if ( $queried_cat ) {
    $total_posts = intval( get_category( get_cat_ID( $queried_cat ) )->count );
} elseif ( $queried_author ) {
    $total_posts = intval( get_the_author_posts() );
} elseif ( $year ) {
    $total_posts = intval( $wp_query->found_posts );
} elseif ( isset( $_GET['s'] ) ) {
    $total_posts = intval( $wp_query->found_posts );
}elseif ( $tag ) {
    $total_posts = intval($wp_query->found_posts);
} else {
    $total_posts = wp_count_posts()->publish;
}

$total_pages = ceil( $total_posts / $queried_per_page );

if ( $year && $monthnum ) {
    $url_string .=  '/' . $year . '/' . $monthnum;
}

if ( $tag ) {
    $url_string .= '/tag/' . $tag;
}

if ( $queried_cat ) {
    $url_string .= '/category/' . $queried_cat;
}

if ( $queried_author ) {
    $url_string .= '/author/' . $queried_author;
}

if ( $queried_page ) {
    $is_first_page = $queried_page === 1;
    $is_last_page = $total_pages <= $queried_page;
    
    

    if ( !$is_first_page ) {
        $page = $queried_page - 1;
        $prev_url_string = $url_string . '/page/' . $page;
        if ( $search ) {
            $prev_url_string .= '/?s=' . $search; 
        }
    }

    if ( !$is_last_page ) {
        $page = $queried_page + 1;
        $next_url_string = $url_string . '/page/' . $page;
        if ( $search ) {
            $next_url_string .= '/?s=' . $search;
        }
    }
}
?>

<?php if ( isset( $prev_url_string ) || isset( $next_url_string ) ) : ?>
    <div class="c-post-navigation">
        <a href="<?php echo $site_url; echo $prev_url_string; ?>" class="c-post-navigation__prev <?php if ( !isset( $prev_url_string ) ) : ?>is-disabled<?php endif; ?>"> 
            <svg class="o-icon o-icon--arrow-left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#arrow-left"></use></svg>
        </a>

        <a href="<?php echo $site_url; echo $next_url_string; ?>" class="c-post-navigation__next <?php if ( !isset( $next_url_string ) ) : ?>is-disabled<?php endif; ?>">
            <svg class="o-icon o-icon--arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/static/symbol/svg/sprite.symbol.svg#arrow-right"></use></svg>
        </a>
    </div>
<?php endif; ?>
