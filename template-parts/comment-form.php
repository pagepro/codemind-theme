<?php 
    $args = array(
        'class_form' => 'f-form f-form--comments js-form-comment',
        'comment_field' => '<div class="row" data-css-spacing="bottom1"><div class="col-xs-12"><div class="f-field js-field--textarea"><div class="f-textarea-wrapper js-valid-textarea-wrapper"><textarea class="f-textarea js-valid-textarea" name="comment" id="comment" placeholder="' . __( 'Add your comment here...', 'codemind' ) . '" required="required"></textarea></div><span class="theme-size-1 message-valid is-hidden is-invalid"></span></div></div></div>',
        'title_reply_before' => '<div class="row" data-css-spacing="bottom1"><div class="col-xs-12"><div class="c-comments-box__header theme-bg-5 theme-color-6"><span class="c-label theme-weight-2 js-reply-message" data-comment-reply="'.__('Leave a Reply', 'codemind').'">',
        'title_reply_after'	=> '</span></div></div></div>',
        'submit_button'	=> '<div class="row"><div class="col-xs-12"><button id="%2$s" name="%1$s" type="submit" class="s-cta-1 theme-bg-3 theme-color-2 js-valid-submit %3$s"><span class="c-label theme-weight-2 theme-size-3">%4$s</span></button></div></div>',
        'submit_field'	=> '<p class="form-submit">%1$s %2$s</p>',
        'logged_in_as' => false,
        'comment_notes_before' => false,
        'fields' => array(
            'author' => '<div class="row" data-css-spacing="bottom1"><div class="col-xs-12 col-sm-4" data-css-spacing="phone(bottom1)"><div class="f-field js-field--name"><div class="f-input-wrapper js-valid-name-wrapper"> <input id="author" name="author" type="text" class="f-input js-valid-name" placeholder="'.__('Name', 'codemind').'" > </div> <span class="theme-size-1 message-valid is-hidden is-invalid"></span> </div></div>',
            'email' => '<div class="col-xs-12 col-sm-4" data-css-spacing="phone(bottom1)"><div class="f-field js-field--email"><div class="f-input-wrapper f-field js-valid-email-wrapper"> <input id="email" name="email" type="email" class="js-valid-email f-input" placeholder="'.__('Email', 'codemind').'" >  </div> <span class="theme-size-1 message-valid is-hidden is-invalid"></span></div></div>',
            'url' => '<div class="col-xs-12 col-sm-4" data-css-spacing="phone(bottom1)"><div class="f-input-wrapper"> <input class="f-input js-valid-url" id="url" name="url" placeholder="'.__('Website URL', 'codemind').'" > </div></div> </div>',
            'cookies' => '<div class="row"><div class="col-xs-12 col-sm-12" data-css-spacing="phone(bottom1)"> <div class="f-checkbox-wrapper"> <input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="yes" /> <label for="wp-comment-cookies-consent"> <span class="c-label theme-weight-1 theme-color-1 theme-font-3"> ' . __( 'Save my name, email, and website in this browser for the next time I comment.', 'codemind') . '</span> </label> </div></div></div>',
        ),
    );
    comment_form($args, $post_id);
?>
