<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package code-mind
 */

?>

    <footer class="l-footer">
        <div class="l-footer__inner">
            <div class="c-copyright theme-font-1 theme-size-2 theme-weight-1 theme-l-height-3">
                <?php
                    if (get_theme_mod('footer_text_block') != "") {
                        echo get_theme_mod('footer_text_block');
                    }
                ?>
            </div>
            <div class="c-newsletter-teaser">
                <span class="c-newsletter-teaser__info theme-font-1 theme-size-2 theme-weight-1 theme-l-height-3"><?php _e('Want to receive weekly updates?', 'code-mind'); ?></span>
                <span class="c-newsletter-teaser__info theme-font-1 theme-size-2 theme-weight-1 theme-l-height-3">
                    <a href="#" class="c-newsletter-teaser__link theme-color-3 js-open-popup">
                        <?php _e('Sign up to the newsletter', 'code-mind'); ?></a>
                    <span><?php _e('and never miss new article.', 'code-mind'); ?></span>
                </span>
            </div>
            <div class="c-popup c-popup--newsletter js-newsletter-popup theme-bg-3">
                <div class="c-popup__inner">
                    <div class="c-popup__content">
                        <form class="f-form c-newsletter js-newsletter-form">
                            <div class="c-newsletter__header">
                                <p class="theme-color-2 theme-font-1 theme-size-4 theme-weight-2 theme-l-height-2">
                                    <?php
                                        if (get_theme_mod('newsletter_text_block') !== "") {
                                            echo get_theme_mod('newsletter_text_block');
                                        }
                                    ?>
                                </p>
                            </div>
                            <div class="c-newsletter__action">
                                <div class="c-action-box c-action-box--wide">
                                    <input name="newsletter-email" id="newsletter-email" class="c-action-box__input theme-font-1 theme-size-3 theme-weight-1 theme-l-height-2 theme-bg-2 theme-color-6 js-newsletter-input" placeholder="Your email here">
                                    <button class="c-action-box__btn c-btn c-btn--newsletter theme-bg-2">
                                        <span class="c-btn btn__content theme-bg-2">
                                            <span class="c-label theme-color-3 t-upper theme-font-1 theme-size-1 theme-weight-2 theme-l-height-2 js-newsletter-sub"><?php _e('Subscribe', 'code-mind'); ?></span>
                                        </span>
                                    </button>
                                </div>
                                <span class="theme-size-1 js-message-valid is-hidden"></span>
                            </div>
                        </form>
                    </div>
                    <a href="#" class="c-popup__close js-close-popup" title="Close popup">
                        <svg class="o-icon o-icon--close">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri() ?>/static/symbol/svg/sprite.symbol.svg#close"></use>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </footer>
</div>
<?php wp_footer(); ?>
</body>

</html>
