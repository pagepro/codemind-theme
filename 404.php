<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package code-mind
 */

get_header();
?>
<main class="l-main">
	<div class="l-inner">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
				<div class="c-post">
					<div class="c-post__header">
						<h2 class="c-post__header__heading">
							<span class="c-post__header__heading theme-font-2 theme-size-9 theme-weight-2 theme-l-height-2">
								<?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'code-mind' ); ?>
							</span>
						</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php
get_footer();
