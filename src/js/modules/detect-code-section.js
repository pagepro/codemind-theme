import $ from 'jquery'

const DetectCodeSection = {
  init: function () {
    const $codeSection = $('.c-code')
    if ($codeSection.length) {
      const $search = $('.js-search-toggler')
      const $socials = $('.js-socials')
      const $window = $(window)
      const activeClass = 'changed-color'

      const searchHeight = $search.outerHeight() - $search.find('.o-icon--search').height() / 2
      let searchOffsetTop = $search.offset().top

      const socialsHeight = $socials.outerHeight()
      const socialsTitleHeight = $socials.find('.c-socials-share__title').height() / 2
      let socialsOffsetTop

      let startBreakpoint = $codeSection.offset().top
      let endBreakpoint = startBreakpoint + $codeSection.height()

      window.onbeforeunload = function () {
        window.scrollTo(0, 0)
      }
      $window.on('resize', function () {
        startBreakpoint = $codeSection.offset().top
        endBreakpoint = startBreakpoint + $codeSection.height()
      })

      $window.on('scroll', function () {
        const scroll = $window.scrollTop()
        if (scroll >= startBreakpoint - searchOffsetTop - searchHeight && scroll < endBreakpoint + searchOffsetTop + searchHeight) {
          $search.addClass(activeClass)
        } else {
          $search.removeClass(activeClass)
        }

        setTimeout(() => {
          socialsOffsetTop = $socials.offset().top

          if (socialsOffsetTop + socialsTitleHeight >= startBreakpoint && socialsOffsetTop < endBreakpoint + socialsHeight / 2) {
            $socials.addClass(activeClass)
          } else {
            $socials.removeClass(activeClass)
          }
        }, 50)
      })
    }
  }
}
DetectCodeSection.init()
