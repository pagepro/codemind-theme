import $ from 'jquery'

const $el = {
  trigger: $('.js-valid-submit'),
  formComment: $('.js-form-comment'),
  textareaWrapper: $('.js-field--textarea'),
  textareaComment: $('.js-valid-textarea'),
  nameComment: $('.js-valid-name'),
  nameWrapper: $('.js-field--name'),
  emailComment: $('.js-valid-email'),
  emailWrapper: $('.js-field--email'),
  urlComment: $('js-valid-url'),
  commentsList: $('.js-comments-list'),
  replyContainer: $('.js-comments-item'),
  parentContainer: $('#comment_parent')
}

const sel = {
  commentItem: '.js-comments-item',
  messageValid: '.message-valid'
}

const classes = {
  isHidden: 'is-hidden'
}

// eslint-disable-next-line
const validEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

const validateComment = e => {
  e.preventDefault()
  let parentValue = $el.parentContainer.val()
  let validate = true
  const $commentItem = $(`[data-commentid=${parentValue}]`).parents(sel.commentItem)
  if ($.trim($el.textareaComment.val()) === '') {
    // eslint-disable-next-line
    const replaceMessage = valid.warningMessage.replace('field-name', $el.textareaComment.attr('name'))
    $el.textareaWrapper.find(sel.messageValid).text(replaceMessage).removeClass(classes.isHidden)
    validate = false
  } else {
    $el.textareaWrapper.find(sel.messageValid).addClass(classes.isHidden)
  }
  if ($el.nameComment.length > 0) {
    if ($.trim($el.nameComment.val()) === '') {
      // eslint-disable-next-line
      const replaceMessage = valid.warningMessage.replace('field-name', $el.nameComment.attr('name'))
      $el.nameWrapper.find(sel.messageValid).text(replaceMessage).removeClass(classes.isHidden)
      validate = false
    } else {
      $el.nameWrapper.find(sel.messageValid).addClass(classes.isHidden)
    }
  }
  if ($el.emailComment.length > 0) {
    if ($.trim($el.emailComment.val()) === '') {
      // eslint-disable-next-line
      const replaceMessage = valid.warningMessage.replace('field-name', $el.emailComment.attr('name'))
      $el.emailWrapper.find(sel.messageValid).text(replaceMessage).removeClass(classes.isHidden)
      validate = false
    } else {
      if (!validEmail.test($el.emailComment.val())) {
        // eslint-disable-next-line
        $el.emailWrapper.find(sel.messageValid).text(valid.incorrectEmail).removeClass(classes.isHidden)
        validate = false
      } else {
        $el.emailWrapper.find(sel.messageValid).addClass(classes.isHidden)
      }
    }
  }
  if (validate) {
    $.ajax({
      // eslint-disable-next-line
      url: valid.ajaxurl,
      method: 'post',
      data: $el.formComment.serialize() + '&action=ajaxcomments'
    })
      .done(function (res) {
        $el.emailComment.val('')
        $el.textareaComment.val('')
        $el.nameComment.val('')
        $el.urlComment.val('')
        if (parentValue === '0') {
          $el.commentsList.prepend(res)
        } else {
          if ($commentItem.find('.c-reply-list').length === 0) {
            $commentItem.append('<ul class="c-reply-list"></ul>')
          }
          $commentItem.find('.c-reply-list').prepend(res)
        }
        $(sel.messageValid).addClass(classes.isHidden)
      })
  }
}

$el.trigger.on('click', validateComment)
