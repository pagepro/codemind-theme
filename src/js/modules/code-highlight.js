import Prism from '../vendor/prism'

const codeElements = document.querySelectorAll('.wp-block-code-mind-code')
if (codeElements) {
  Array.from(codeElements).forEach(pre => {
    const code = pre.querySelector('code')
    const lang = code.classList[0]
    code.classList = `language-${lang}`
  })
  setTimeout(() => {
    Array.from(codeElements).forEach(pre => {
      const code = pre.querySelector('code')
      Prism.highlightElement(code)
    })
  }, 500)
}
