import $ from 'jquery'

const $el = {
  popupTrigger: $('.js-open-popup'),
  popup: $('.js-newsletter-popup'),
  closeButton: $('.js-close-popup'),
  body: $('body')
}

const openPopup = e => {
  e.preventDefault()
  e.stopPropagation()
  $el.popup.fadeIn('slow')
}

const closePopup = e => {
  e.preventDefault()
  $el.popup.fadeOut('slow')
}

const closePopupOnBody = e => {
  if (!$(e.target).closest($el.popup).length) {
    $el.popup.fadeOut('slow')
  } else {
    e.stopPropagation()
  }
}

$el.popupTrigger.on('click', openPopup)
$el.body.on('click', closePopupOnBody)
$el.closeButton.on('click', closePopup)
