import $ from 'jquery'

const $trigger = $('.js-anim-rewind')

const SmoothRewind = {
  init: function () {
    $trigger.on('click', function (e) {
      e.preventDefault()
      $('html, body').animate({
        scrollTop: $($(e.currentTarget).attr('href')).offset().top - $('.l-header').height() / 2
      }, 800)
    })
  }
}
SmoothRewind.init()
