import $ from 'jquery'
import { BREAKPOINTS } from './../utils'

const StickyHeader = {
  init: function () {
    const stickyClass = 'is-sticky'

    const $header = $('.js-sticky-header')
    const $window = $(window)
    let sizeGutter = 180

    if ($window.width() <= BREAKPOINTS.phone) {
      sizeGutter = 60
    }
    if ($window.width() <= BREAKPOINTS.phoneSmall) {
      sizeGutter = 40
    }

    const $headerAlt = $('.js-sticky-header-alt')
    const $headerMenuToggler = $('.js-header-menu-toggler')
    const $headerTitle = $('.js-header-title')
    const $headerTools = $('.js-header-tools')
    const subHeroHeight = $('.js-sub-hero').height()

    $window.on('scroll', function () {
      const distance = $window.scrollTop()
      $header.toggleClass(stickyClass, distance > sizeGutter)
      $headerTitle.toggleClass(stickyClass, distance > sizeGutter)

      $headerAlt.toggleClass(stickyClass, distance > subHeroHeight)
      $headerMenuToggler.toggleClass(stickyClass, distance > subHeroHeight)
      $headerTools.toggleClass(stickyClass, distance > subHeroHeight)
    })
  }
}
StickyHeader.init()
