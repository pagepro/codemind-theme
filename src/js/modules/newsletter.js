import $ from 'jquery'

// `eslint-disable-next-line` is used to disable eslint on a specific line. Some of the objects are from 'wp_localize_script

const $el = {
  form: $('.js-newsletter-form')
}

const classes = {
  isHidden: 'is-hidden',
  isInvalid: 'is-invalid',
  isValid: 'is-valid'
}

const makeInValid = message => {
  $el.message.text(message).removeClass(classes.isHidden).removeClass(classes.isValid).addClass(classes.isInvalid)
}

const makeValid = message => {
  $el.message.text(message).removeClass(classes.isInvalid).addClass(classes.isValid).removeClass(classes.isHidden)
}

// eslint-disable-next-line
const validEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

const handleNewsletterForm = e => {
  e.preventDefault()

  const $form = $(e.currentTarget)
  const $email = $form.find('.js-newsletter-input')
  $el.message = $form.find('.js-message-valid')

  let validate = true

  if ($.trim($email.val()) === '') {
    validate = false
    // eslint-disable-next-line
    makeInValid(valid.emptyEmail)
  } else {
    // eslint-disable-next-line
    if (!validEmail.test($email.val())) {
      validate = false
      // eslint-disable-next-line
      makeInValid(valid.incorrectEmail)
    } else {
      // eslint-disable-next-line
      makeValid(valid.correctEmail)
    }
  }

  if (validate) {
    $.ajax({
      // eslint-disable-next-line
      url: valid.ajaxurl,
      method: 'post',
      data: $form.serialize() + '&action=newsletter'
    })
      .done(res => {
        if (res !== '') {
          $email.val('')
        } else {
          // eslint-disable-next-line
          makeInValid(valid.error)
        }
      })
  }
}

$el.form.each(function () {
  const $form = $(this)
  $form.on('submit', handleNewsletterForm)
})
