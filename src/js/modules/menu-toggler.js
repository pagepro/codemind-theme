import $ from 'jquery'

const css = {
  activeClass: 'is-active',
  openMenu: 'open-menu'
}
const $el = {
  menuToggler: $('.js-menu-toggler'),
  menuNav: $('.js-menu-nav'),
  closeToggler: $('.js-menu-close'),
  body: $('body'),
  menuBox: $('.js-header-menu-box')
}

const toggleMenu = e => {
  e.preventDefault()
  e.stopPropagation()
  $el.menuToggler.toggleClass(css.activeClass)
  $el.menuNav.toggleClass(css.activeClass)
  $el.body.toggleClass(css.openMenu)
  $el.closeToggler.css('margin-top', $el.menuBox.height() / 2 - $el.menuToggler.height() / 2)
}

const closeMenu = e => {
  if ($el.menuNav.hasClass(css.activeClass)) {
    e.preventDefault()
    e.stopPropagation()
    $el.menuToggler.removeClass(css.activeClass)
    $el.body.removeClass(css.openMenu)
    $el.menuNav.removeClass(css.activeClass)
  }
}

const closeMenuOnBody = e => {
  if ($el.body.hasClass(css.openMenu)) {
    $el.menuToggler.removeClass(css.activeClass)
    $el.body.removeClass(css.openMenu)
    $el.menuNav.removeClass(css.activeClass)
  }
}

const noClose = e => {
  e.stopPropagation()
}

$el.menuToggler.on('click', toggleMenu)
$el.closeToggler.on('click', closeMenu)
$el.body.on('click', closeMenuOnBody)
$el.menuNav.on('click', noClose)

