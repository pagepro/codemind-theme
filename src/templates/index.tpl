<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">
        <title>{% block title %}{{ title }}{% endblock %}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        {% include "head/head-links.tpl" %}
        {% include "head/styles.tpl" %}
    </head>
    <body>
        {% import "partials/header.tpl" as l_header %}
        {{ l_header.render() }}
        <main class="l-main">
            <div class="l-inner">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
                        <div class="c-post">
                            <div class="c-post__categories">
                                <ul class="c-categories-list">
                                    <li class="c-categories-list__item">
                                        <a href="category.html" class="c-categories-list__item__link theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 theme-color-4 u-decor-bottom">Design</a>
                                    </li>
                                    <li class="c-categories-list__item">
                                        <a href="category.html" class="c-categories-list__item__link theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 theme-color-4 u-decor-bottom">User Experience</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="c-post__header">
                                <h2 class="c-post__header__heading">
                                    <a href="post-details.html" class="c-post__header__heading__link theme-font-2 theme-size-9 theme-weight-2 theme-l-height-2">We have two really good<br class="u-hide"> reasons to remove toxins from devices.</a>
                                </h2>
                                <div class="c-post__header__desc">
                                    <div class="c-post-info">
                                        <span class="c-post-info__author theme-color-4">
                                            <span class="c-post-info__author__pre theme-font-1 theme-size-2 theme-style-2 theme-weight-3 theme-l-height-3">by</span>
                                            <a class="c-post-info__author__name" href="author.html"><span class="theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 t-upper t-space-wide">Chris lojniewski</span></a>
                                        </span>
                                        <a href="#" class="c-post-info__date">
                                            <time class="c-post-info__date__info theme-color-4 t-space-wide t-upper theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3" datetime="2016-11-05 00:00">05 november 2016</time>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="c-post__content">
                                <div class="c-post__content__title">
                                    <p class="theme-font-1 theme-size-5 theme-l-height-3 t-weight-1">We have two really good reasons to remove toxins from devices. People and the planet.</p>
                                </div>
                                <div class="c-post__content__article">
                                    <p class="theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4">
                                        We started our Full Material Disclosure program to identify all the substances we use in all the parts we use. We’ve already looked at more than <span class="theme-color-3">10,000 individual</span> components, and we get data on more parts every day. We assess the different chemicals in those components using 18 different criteria. This helps us understand their effect on our health and on the environment.
                                    </p>
                                </div>
                                <a href="post-details.html" class="c-read-more">
                                    <span class="theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 t-upper theme-color-3">Read More</span>
                                </a>
                            </div>
                        </div>
                        <div class="d-decor-parent d-decor-parent--with-big-gutter">
                            <div class="d-decor"></div>
                        </div>
                        <div class="c-post">
                            <div class="c-post__categories">
                                <ul class="c-categories-list">
                                    <li class="c-categories-list__item">
                                        <a href="category.html" class="c-categories-list__item__link theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 theme-color-4 u-decor-bottom">Design</a>
                                    </li>
                                    <li class="c-categories-list__item">
                                        <a href="category.html" class="c-categories-list__item__link theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 theme-color-4 u-decor-bottom">User Experience</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="c-post__header">
                                <h2 class="c-post__header__heading">
                                    <a href="post-details.html" class="c-post__header__heading__link theme-font-2 theme-size-9 theme-weight-2 theme-l-height-2">We have two really good<br class="u-hide"> reasons to remove toxins from devices.</a>
                                </h2>
                                <div class="c-post__header__desc">
                                    <div class="c-post-info">
                                        <span class="c-post-info__author theme-color-4">
                                            <span class="c-post-info__author__pre theme-font-1 theme-size-2 theme-style-2 theme-weight-3 theme-l-height-3">by</span>
                                            <a class="c-post-info__author__name" href="author.html"><span class="theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 t-upper t-space-wide">Chris lojniewski</span></a>
                                        </span>
                                        <a href="#" class="c-post-info__date">
                                            <time class="c-post-info__date__info theme-color-4 t-space-wide t-upper theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3" datetime="2016-11-05 00:00">05 november 2016</time>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="c-post__content">
                                <div class="c-post__content__title">
                                    <p class="theme-font-1 theme-size-5 theme-l-height-3 t-weight-1">We have two really good reasons to remove toxins from devices. People and the planet.</p>
                                </div>
                                <div class="c-post__content__article">
                                    <p class="theme-font-1 theme-size-4 theme-weight-1 theme-l-height-4">
                                        We started our Full Material Disclosure program to identify all the substances we use in all the parts we use. We’ve already looked at more than <span class="theme-color-3">10,000 individual</span> components, and we get data on more parts every day. We assess the different chemicals in those components using 18 different criteria. This helps us understand their effect on our health and on the environment.
                                    </p>
                                </div>
                                <a href="post-details.html" class="c-read-more">
                                    <span class="theme-font-1 theme-size-2 theme-weight-2 theme-l-height-3 t-upper theme-color-3">Read More</span>
                                </a>
                            </div>
                        </div>
                        <div class="d-decor-parent d-decor-parent--with-big-gutter">
                            <div class="d-decor"></div>
                        </div>
                        <div class="c-post-navigation">
                            <a href="#" class="c-post-navigation__prev is-disabled">
                                <svg class="o-icon o-icon--arrow-left">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#arrow-left"></use>
                                </svg>
                           </a>
                            <a href="#" class="c-post-navigation__next">
                                <svg class="o-icon o-icon--arrow-right">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./static/symbol/svg/sprite.symbol.svg#arrow-right"></use>
                                </svg>
                           </a>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        {% include "partials/footer.tpl" %}
        <script src="./static/js/app.js"></script>
    </body>
</html>