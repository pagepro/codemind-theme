<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">
        <title>{% block title %}{{ title }}{% endblock %}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        {% include "head/head-links.tpl" %}
        {% include "head/styles.tpl" %}
    </head>
    <body>
        {% import "partials/header.tpl" as l_header %}
        {{ l_header.render() }}
        <main class="l-main">
            <div class="l-inner">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
                        <article class="c-cms">
                            <div class="c-cms__inner">
                                <h1>The biggest headline.</h1>
                                <p>Sed et libero eros. Praesent quis ipsum massa, nec aliquet tellus. Etiam purus neque, varius eget elementum vel, hendrerit id enim. Vestibulum non ultricies dui. Donec bibendum accumsan ligula sit amet consectetur. Proin mattis sapien eget enim cursus in mattis dui rhoncus. Aenean suscipit vehicula urna. Praesent eget nisl libero. Pellentesque accumsan lacus vel lorem euismod non posuere ipsum fringilla</p>
                                <h4>Two columns content</h4>
                                <div class="twocolumn">
                                    <p class="half-width">Nullam tincidunt euismod eros. Nullam sodales, tortor vel lacinia varius, orci urna consectetur neque, sit amet accumsan lacus augue at eros. Nullam pellentesque vel mi nec congue. Donec sit amet auctor erat. In hac habitasse platea dictumst.</p>
                                    <p class="half-width">Aenean arcu nibh, auctor vitae elit nec, ultrices auctor elit. Nam pulvinar dignissim leo eget egestas. Curabitur nec arcu mollis, ultrices lacus vel, mollis est. Nunc id elit metus. Suspendisse ac lacus ut lacus cursus euismod ut et felis.</p>
                                </div>
                                <p>&nbsp;</p>
                                <h4>Emphasis text</h4>
                                <p class="emphasis">Aliquet elit, eget tincidunt nibh pulvinar a. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Lorem ipsum dolor sit amet.</p>
                                <p>&nbsp;</p>
                                <h4>Dropcap paragraph</h4>
                                <p style="position: relative; z-index: 0;"><span class="dropcap" data-dropcap="P">P</span>ultrices auctor elit. Nam pulvinar dignissim leo eget egestas. Curabitur nec arcu mollis, ultrices lacus vel, mollis est. Nunc id elit metus. Suspendisse ac lacus ut lacus cursus euismod ut et felis tortor vel lacinia varius, orci urna consectetur neque, sit amet accumsan lacus augue at eros. Nullam pellentesque vel mi nec congue. Donec sit amet auctor erat. In hac habitasse platea dictums. Aenean suscipit vehicula urna. Praesent eget nisl libero. Pellentesque accumsan lacus vel lorem euismod non posuere ipsum fringilla.</p>
                                <p>&nbsp;</p>
                                <h4>Blockquotes</h4>
                                <p>Single line blockquote:</p>
                                <blockquote><p>Stay hungry. Stay foolish.</p></blockquote>
                                <p>Multi line blockquote with a cite reference:</p>
                                <blockquote><p>People think focus means saying yes to the thing you’ve got to focus on. But that’s not what it means at all. It means saying no to the hundred other good ideas that there are. You have to pick carefully. I’m actually as proud of the things we haven’t done as the things I have done. Innovation is saying no to 1,000 things.</p>
                                <p><cite>Steve Jobs – Apple Worldwide Developers' Conference, 1997</cite></p></blockquote>
                                <p>That last is no more than a foot high, and about seven paces across, a mere flat top of a grey rock which smokes like a hot cinder after a shower, and where no man would care to venture a naked sole before sunset. On the Little Isabel an old ragged palm, with a thick bulging trunk rough with spines, a very witch amongst palm trees, rustles a dismal bunch of dead leaves above the coarse sand. The Great Isabel has a spring of fresh water issuing from the overgrown side of a ravine. Resembling an emerald green wedge of land a mile long, and laid flat upon the sea, it bears two forest trees standing close together, with a wide spread of shade at the foot of their smooth trunks. A ravine extending the whole length of the island is full of bushes; and presenting a deep tangled cleft on the high side spreads itself out on the other into a shallow depression abutting on a small strip of sandy shore.</p>
                                <blockquote class="pull-right"><p>This is a right aligned blockquote. It is effortless in its beauty</p></blockquote>
                                <p>That last is no more than a foot high, and about seven paces across, a mere flat top of a grey rock which smokes like a hot cinder after a shower, and where no man would care to venture a naked sole before sunset. On the Little Isabel an old ragged palm, with a thick bulging trunk rough with spines, a very witch amongst palm trees, rustles a dismal bunch of dead leaves above the coarse sand. The Great Isabel has a spring of fresh water issuing from the overgrown side of a ravine. Resembling an emerald green wedge of land a mile long, and laid flat upon the sea, it bears two forest trees standing close together, with a wide spread of shade at the foot of their smooth trunks. A ravine extending the whole length of the island is full of bushes; and presenting a deep tangled cleft on the high side spreads itself out on the other into a shallow depression abutting on a small strip of sandy shore.</p>
                                <blockquote class="pull-left"><p>This is a left aligned blockquote. It gives the page a harmonic visual rhythm.</p></blockquote>
                                <p>That last is no more than a foot high, and about seven paces across, a mere flat top of a grey rock which smokes like a hot cinder after a shower, and where no man would care to venture a naked sole before sunset. On the Little Isabel an old ragged palm, with a thick bulging trunk rough with spines, a very witch amongst palm trees, rustles a dismal bunch of dead leaves above the coarse sand. The Great Isabel has a spring of fresh water issuing from the overgrown side of a ravine. Resembling an emerald green wedge of land a mile long, and laid flat upon the sea, it bears two forest trees standing close together, with a wide spread of shade at the foot of their smooth trunks. A ravine extending the whole length of the island is full of bushes; and presenting a deep tangled cleft on the high side spreads itself out on the other into a shallow depression abutting on a small strip of sandy shore.</p>
                                <p style="text-align: center;">So if you ever need anything like, let’s say, Ultimate Awesome WordPress Premium Themes, please visit this <a title="Link" href="#">link</a></p>
                                <p>&nbsp;</p>
                                <h5>Smaller headline</h5>
                                <p>Donec sollicitudin molestie malesuada. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec rutrum congue leo eget malesuada. Nulla quis lorem ut libero malesuada feugiat. Proin eget tortor risus. Curabitur aliquet quam id dui posuere blandit. Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. Donec rutrum congue leo eget malesuada.</p>
                                <h6>Smallest headline</h6>
                                <p>Nam id nisl mauris. Quisque auctor metus sit amet risus dignissim blandit. Fusce enim augue, ullamcorper quis urna id, condimentum laoreet mi. <strong>Vestibulum pretium consectetur scelerisque</strong>. In pharetra, lorem hendrerit congue placerat, eros risus molestie libero, fermentum rhoncus nisl arcu in dolor.</p>
                                <p>&nbsp;</p>
                                <h4>Image alignment</h4>
                                <p><img class="alignleft size-full" src="../static/img/pic_nature-1.jpg" alt="Picture" width="340" height="240">The rest of this paragraph is filler for the sake of seeing the text wrap around the 150×150 image, which is <em>left aligned</em>. As you can see the should be some space above, below, and to the right of the image. The text should not be creeping on the image. Creeping is just not right. Images need breathing room too. Let them speak like you words. Let them do their jobs without any hassle from the text. In about one more sentence here, we’ll see that the text moves from the right of the image down below the image in seamless transition. Again, letting the do it’s thang. Mission accomplished!</p>
                                <p>&nbsp;</p>
                                <p><img class="alignright size-full" src="../static/img/pic_nature-2.jpg" alt="Picture 1" width="340" height="240">And now we’re going to shift things to the <em>right align</em>. Again, there should be plenty of room above, below, and to the left of the image. Just look at him there… Hey guy! Way to rock that right side. I don’t care what the left aligned&nbsp;image says, you look great.</p>
                                <p>Don’t let anyone else tell you differently.&nbsp;In just a bit here, you should see the text start to wrap below the right aligned image and settle in nicely. There should still be plenty of room and everything should be sitting pretty. Yeah… Just like that. It never felt so good to be right.</p>
                                <p>&nbsp;</p>
                                <h4>One big great image</h4>
                                <figure class="wp-caption aligncenter">
                                    <img class="size-full" src="../static/img/pic_nature-3.jpg" alt="Picture">
                                    <figcaption class="wp-caption-text">One small great caption</figcaption>
                                </figure>
                                <p>Okay, we’re done with the fun part and pretty images, now let’s get on to business. Yup, it’s that time for tables and lists folks.</p>
                                <h4>Tables</h4>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Employee</th>
                                            <th>Salary</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th><a href="http://example.org/">John Doe</a></th>
                                            <td>$1</td>
                                            <td>Because that’s all Steve Jobs needed for a salary.</td>
                                        </tr>
                                        <tr>
                                            <th><a href="http://example.org/">Jane Doe</a></th>
                                            <td>$100K</td>
                                            <td>For all the blogging she does.</td>
                                        </tr>
                                        <tr>
                                            <th><a href="http://example.org/">Fred Bloggs</a></th>
                                            <td>$100M</td>
                                            <td>Pictures are worth a thousand words, right? So Jane x 1,000.</td>
                                        </tr>
                                        <tr>
                                            <th><a href="http://example.org/">Jane Bloggs</a></th>
                                            <td>$100B</td>
                                            <td>With hair like that?! Enough said…</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <p>…or</p>
                                <h3>Lists</h3>
                                <ul>
                                    <li>List item one
                                        <ul>
                                            <li>List item one
                                                <ul>
                                                    <li>List item one</li>
                                                    <li>List item two</li>
                                                    <li>List item three</li>
                                                    <li>List item four</li>
                                                </ul>
                                            </li>
                                            <li>List item two</li>
                                            <li>List item three</li>
                                            <li>List item four</li>
                                        </ul>
                                    </li>
                                    <li>List item two</li>
                                    <li>List item three</li>
                                    <li>List item four</li>
                                </ul>
                                <h3>and Ordered List (Nested)</h3>
                                <ol>
                                    <li>List item one
                                        <ol>
                                            <li>List item one
                                                <ol>
                                                    <li>List item one</li>
                                                    <li>List item two</li>
                                                    <li>List item three</li>
                                                    <li>List item four</li>
                                                </ol>
                                            </li>
                                            <li>List item two</li>
                                            <li>List item three</li>
                                            <li>List item four</li>
                                        </ol>
                                    </li>
                                    <li>List item two</li>
                                    <li>List item three</li>
                                    <li>List item four</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p>…even some on-page coding</p>
                                <pre>.post-title {
margin: 0 0 5px;
font-weight: bold;
font-size: 38px;
line-height: 1.2;
and here's a line of some really, really, really, really long text, just to see how the PRE tag handles it and to find how it overflows;
}</pre>
                                <h4 style="text-align: center;">
                                Thanks for scrolling!</h4>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </main>
        {% include "partials/footer.tpl" %}
        <script src="./static/js/app.js"></script>
    </body>
</html>